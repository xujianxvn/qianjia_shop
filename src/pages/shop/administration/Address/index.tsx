import { PageContainer } from '@ant-design/pro-layout';
import React, { useEffect } from 'react';
import { Collapse, Form } from 'antd';
import { useState } from 'react';
import MyForm from '@/components/MyForm';
import api from '@/services';

export default function index() {
  const { Panel } = Collapse;
  const text = `• 通过地图定位来填充联系地址或通过点击"查找按钮"来锁定地图上的具体位置`;

  const startTime = 1;
  const onFinish = async (values: any) => {
    console.log(11, values);
    const data: any = await api.reqShopEdit(values);
    console.log(data);
  };

  const onFinishFailed = (values: any) => {
    console.log(22, values);
  };

  const formConfig = {
    config: {
      name: 'formName',
      labelCol: { span: 6 },
      wrapperCol: { span: 14 },
    },
    formItems: [
      {
        id: 1,
        config: {
          label: '联系人姓名',
          name: 'name',
          value: 111,
          rules: [{ required: true }],
        },
        item: {
          config: {
            placeholder: '请输入文本',
            value: 111,
          },
          eleType: 'text',
        },
      },
      {
        id: 2,
        config: {
          label: '联系人手机号',
          name: 'mobile',
          rules: [{ required: true }],
        },
        item: {
          config: {
            placeholder: '请输入手机号',
            rules: [{ required: true }],
          },
          eleType: 'text',
        },
      },
      {
        id: 3,
        config: {
          label: '联系人电话',
          name: 'telephone',
        },
        item: {
          config: {
            placeholder: '请输入电话',
          },
          eleType: 'text',
        },
      },
      {
        id: 4,
        config: {
          label: '联系地址',
          name: 'full_address',
          rules: [{ required: true }],
        },
        item: {
          config: {
            placeholder: '请选择',
            options: [
              {
                value: '四川',
                label: '四川',
                children: [
                  {
                    value: '成都市',
                    label: '成都市',
                    children: [{ value: '锦江区', label: '锦江区' }],
                  },
                ],
              },
            ],
          },
          eleType: 'cascader',
        },
      },
      {
        id: 5,
        config: {
          label: '详细地址',
          name: 'address',
          rules: [{ required: true }],
        },
        item: {
          config: {
            placeholder: '请输入详细地址',
          },
          eleType: 'text',
        },
      },
      {
        id: 6,
        config: {
          label: 'QQ',
          name: 'qq',
        },
        item: {
          config: {
            placeholder: '',
          },
          eleType: 'text',
        },
      },
      {
        id: 7,
        config: {
          label: '阿里旺旺',
          name: 'ww',
        },
        item: {
          config: {
            placeholder: '',
          },
          eleType: 'text',
        },
      },
      {
        id: 8,
        config: {
          label: '邮箱',
          name: 'email',
        },
        item: {
          config: {
            placeholder: '请输入邮箱',
            rules: [{ required: true }],
          },
          eleType: 'text',
        },
      },
      {
        id: 9,
        config: {
          label: '工作日',
          name: 'work_week',
        },
        item: {
          config: {
            options: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
          },
          eleType: 'checkboxGroup',
        },
      },
      {
        id: 11,
        config: {
          wrapperCol: { offset: 6, span: 14 },
        },
        item: {
          config: {
            type: 'primary',
            htmlType: 'submit',
          },
          eleType: 'button',
          content: '提交',
        },
      },
    ],
  };

  // const [list, setList] = useState('');
  const [form] = Form.useForm();

  const reqShopInfo = async () => {
    const data: any = await api.reqShopInfo();
    if (data.data.code !== 0) return
    data.data.data.work_week = data.data.data.work_week.split(',')
    data.data.data.full_address = data.data.data.full_address.split(',')
    console.log(data.data.data);
    
    form.setFieldsValue(data.data.data);
  };

  useEffect(() => { reqShopInfo() }, []);

  return (
    <div>
      <PageContainer
        style={{ height: '50px', overflow: 'hidden' }}
      ></PageContainer>
      <Collapse
        defaultActiveKey={['1']}
        ghost
        style={{ border: '1px solid #ddd' }}
      >
        <Panel header="操作提示" key="1">
          <p>{text}</p>
        </Panel>
      </Collapse>
      ,{/* ----------提示end-- ----*/}
      <MyForm
        formConfig={formConfig}
        form={form}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      />
    </div>
  );
}
