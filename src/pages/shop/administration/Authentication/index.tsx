import { PageContainer } from '@ant-design/pro-layout';
import React, { useEffect, useState } from 'react';
import { Table } from 'antd';
import Style from '@/pages/shop/administration/Authentication/index.less';
import api from '@/services';

export default function index() {
  const [name, setName] = useState('');
  const [cardNo, setCardNo] = useState('');
  const [cardHand, setCardHand] = useState('');
  const [cardFront, setCardFront] = useState('');
  const [cardBack, setCardBack] = useState('');
  const reqAuthInfo = async () => {
    const data: any = await api.reqAuthInfo();
    console.log(data);
    const card_name = data.data.data[0].auth_card_name;
    const card_no = data.data.data[0].auth_card_no;
    const auth_card_hand = data.data.data[0].auth_card_hand;
    const auth_card_front = data.data.data[0].auth_card_front;
    const auth_card_back = data.data.data[0].auth_card_back;
    setName(card_name);
    setCardNo(card_no);
    setCardHand(auth_card_hand);
    setCardFront(auth_card_front);
    setCardBack(auth_card_back);
  };
  useEffect(() => {
    reqAuthInfo();
  }, []);

  return (
    <div>
      <PageContainer
        style={{ height: '50px', overflow: 'hidden' }}
      ></PageContainer>

      <table className={Style.table}>
        <tbody>
          <tr style={{}}>
            <th colSpan="2" className={Style.th}>
              认证类型
            </th>
          </tr>
          <tr style={{ border: '1px solid #ddd' }}>
            <td className={Style.tdleft}>认证类型:</td>
            <td className={Style.td}>个人认证</td>
          </tr>
        </tbody>
      </table>

      <table className={Style.table}>
        <tbody>
          <tr style={{}}>
            <th colSpan="2" className={Style.th}>
              联系人信息
            </th>
          </tr>
          <tr style={{ border: '1px solid #ddd' }}>
            <td className={Style.tdleft}>联系人姓名：</td>
            <td className={Style.td}>{name}</td>
          </tr>
          <tr style={{ border: '1px solid #ddd' }}>
            <td className={Style.tdleft}>联系人手机号：</td>
            <td className={Style.td}>13611123133</td>
          </tr>
          <tr style={{ border: '1px solid #ddd' }}>
            <td className={Style.tdleft}>联系人身份证号：</td>
            <td className={Style.td}>{cardNo}</td>
          </tr>
          <tr style={{ border: '1px solid #ddd' }}>
            <td className={Style.tdleft}>联系人手持身份证：</td>
            <td className={Style.tdimg}>
              <img className={Style.img} src={cardHand}></img>
            </td>
          </tr>

          <tr style={{ border: '1px solid #ddd' }}>
            <td className={Style.tdleft}>联系人身份证正面：</td>
            <td className={Style.tdimg}>
              <img className={Style.img} src={cardFront}></img>
            </td>
          </tr>
          <tr>
            <td className={Style.tdleft}>联系人身份证反面：</td>
            <td className={Style.tdimg}>
              <img className={Style.img} src={cardBack}></img>{' '}
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
