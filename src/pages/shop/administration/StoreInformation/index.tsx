import React, { useEffect, useState } from 'react';
import { Table } from 'antd';
// import { Goodscategory } from '@/http';
import { Button } from 'antd';
import { Collapse, Form } from 'antd';
import { history } from 'umi';
import { PageContainer } from '@ant-design/pro-layout';
import MyForm from '@/components/MyForm';
import api from '@/services';
import index from '@/components/Personal/index';
const { Panel } = Collapse;
// function callback() {}

const rowSelection = {
  onChange: (selectedRowKeys: any, selectedRows: any) => {
    console.log(
      `selectedRowKeys: ${selectedRowKeys}`,
      'selectedRows: ',
      selectedRows,
    );
  },
};

export default function categary() {

  const [form] = Form.useForm();

  const reqShopInfo = async () => {
    const data: any = await api.reqShopInfo();
    console.log(data);
    const {shop_name, description, keywords,} = data.data.data
    form.setFieldsValue({shop_name, description, keywords})
  };

  useEffect(() => {reqShopInfo()}, []);

  const normFile = (e: any) => {
    console.log('Upload event:', e);
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  const formConfig = {
    config: {
      name: 'formName',
      labelCol: { span: 6 },
      wrapperCol: { span: 14 },
    },
    formItems: [
      {
        id: 0,
        config: {
          label: '店铺名称',
          name: 'shop_name',
        },
        item: {
          config: {
            
          },
          eleType: 'text',
        },
      },
      {
        id: 1,
        config: {
          label: '店铺logo',
          name: 'logo',
          valuePropName: 'fileList',
          getValueFromEvent: normFile,
          rules: [{ required: true }],
        },
        item: {
          config: {
            maxCount: 1,
            listType: 'picture-card',
          },
          eleType: 'upload',
        },
      },
      {
        id: 2,
        config: {
          label: '店铺头像',
          name: 'avatar',
          valuePropName: 'fileList',
          getValueFromEvent: normFile,
          rules: [{ required: true }],
        },
        item: {
          config: {
            maxCount: 1,
            listType: 'picture-card',
          },
          eleType: 'upload',
        },
      },
      {
        id: 3,
        config: {
          label: '店铺大图',
          name: 'banner',
          valuePropName: 'fileList',
          getValueFromEvent: normFile,
          rules: [{ required: true }],
        },
        item: {
          config: {
            maxCount: 1,
            listType: 'picture-card',
          },
          eleType: 'upload',
        },
      },
      {
        id: 4,
        config: {
          label: '店铺简介',
          name: 'description',
          rules: [{ required: true }],
        },
        item: {
          config: {
            placeholder: '请输入内容',
          },
          eleType: 'textarea',
        },
      },
      {
        id: 5,
        config: {
          label: '店铺关键字',
          name: 'keywords',
          rules: [{ required: true }],
        },
        item: {
          config: {
            placeholder: '请输入文本',
          },
          eleType: 'text',
        },
      },
      {
        id: 6,
        config: {
          wrapperCol: { offset: 6, span: 14 },
        },
        item: {
          config: {
            type: 'primary',
            htmlType: 'submit',
          },
          eleType: 'button',
          content: '提交',
        },
      },
    ],
  };

  const toImageString = (imgArr: Array<any>) => {
    if (Array.isArray(imgArr))
      return imgArr.map((item: any) => item.response.data.path).join(',');
  };

  const onFinish = async (values: any) => {
    values.logo = toImageString(values.logo);
    values.avatar = toImageString(values.avatar);
    values.banner = toImageString(values.banner);
    console.log(11, values);
    const data: any = await api.reqShopEdit(values);
    console.log(data);
  };

  const onFinishFailed = (values: any) => {
    console.log(values);
  };

  return (
      <PageContainer>
        <MyForm
          formConfig={formConfig}
          form={form}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        />
      </PageContainer>
  );
}
