import { PageContainer } from '@ant-design/pro-layout';
import React from 'react';
import Style from '@/pages/shop/administration/settled/Query/index.less';
import { history } from 'umi';

export default function index() {
  const torenewal = () => {
    console.log(history);
    history.push('Renewal');
  };
  return (
    <div>
      <PageContainer
        style={{
          height: '40px',
          overflow: 'hidden',
          borderBottom: '2px solid #f7f7f7',
          paddingBottom: '50px',
        }}
      ></PageContainer>
      <div className={Style.content}>
        <div
          style={{
            border: '1px solid #ddd',
            borderRadius: '1px',
            padding: '10px 20px',
          }}
        >
          <div>操作提示</div>
          <ul style={{ color: '#999', font: '14px' }}>
            <li>1、店铺到期30日内可以申请续签</li>
            <li>2、请认准官方收款账户，支付凭据上传之后请联系官方客服人员</li>
          </ul>
        </div>
        <div className={Style.settled_money}>
          <span style={{}}></span>店铺信息
        </div>
        <div style={{ display: 'flex', marginLeft: '50px' }}>
          <div style={{ display: 'flex' }}>
            <div>
              <img
                src="https://multi.niuteam.cn/public/static/img/default_shop.png"
                style={{ width: '40px', height: '40px' }}
              />
            </div>
            <div
              style={{
                fontSize: '14px',
                fontWeight: 600,
                color: '#333333',
                marginLeft: '10px',
              }}
            >
              <div>
                店铺名称:
                <span
                  style={{
                    fontWeight: 500,
                    marginLeft: '5px',
                    color: '#666666',
                    marginBottom: '5px',
                  }}
                >
                  好又来
                </span>
              </div>
              <div>
                开店套餐:
                <span
                  style={{
                    fontWeight: 500,
                    marginLeft: '5px',
                    color: '#666666',
                  }}
                >
                  旗舰店铺
                </span>
              </div>
            </div>
          </div>
          <div>
            <div
              style={{
                fontSize: '14px',
                fontWeight: 600,
                color: '#333333',
                marginLeft: '150px',
              }}
            >
              <div>
                店铺分类:
                <span
                  style={{
                    fontWeight: 500,
                    marginLeft: '5px',
                    color: '#666666',
                    marginBottom: '5px',
                  }}
                >
                  直营专卖店
                </span>
              </div>
              <div>
                到期时间:
                <span
                  style={{
                    fontWeight: 500,
                    marginLeft: '5px',
                    color: '#666666',
                  }}
                >
                  2021-09-03 08:38:00
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className={Style.settled_money}>
          <span style={{}}></span>入驻时间
        </div>
        <div style={{ display: 'flex', padding: '10px', marginLeft: '40px' }}>
          <div>
            <div className={Style.settled_word}>入驻费用</div>
            <div className={Style.settled_num}>0.00</div>
          </div>
          <div style={{ margin: '0 300px' }}>
            <div className={Style.settled_word}>保证金</div>
            <div className={Style.settled_num}>0.00</div>
          </div>
          <div>
            <div className={Style.settled_word}>
              到期时间{' '}
              <span
                style={{ fontSize: '12px', color: '#ff6a00', margin: '0 10px' }}
                onClick={torenewal}
              >
                申请续签
              </span>
            </div>
            <div className={Style.settled_num}>2021-09-01 19:17:33</div>
          </div>
        </div>
      </div>
    </div>
  );
}
