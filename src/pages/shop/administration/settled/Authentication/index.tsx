import React, { useState } from 'react';
import Company from '@/components/Company';
import Personal from '@/components/Personal';
import Style from '@/pages/shop/administration/settled/Authentication/index.less';
import { PageContainer } from '@ant-design/pro-layout';
import { Button, Radio, Form } from 'antd';

export default function index() {
  const [state, setState] = useState(1);
  const onChange = (e: any) => {
    console.log(e.target.value);
    setState(e.target.value);
    const a = e.target.value;
    if (e.target.value == 1) {
      return;
    }
  };
  return (
    <div>
      <PageContainer
        style={{
          height: '50px',
          overflow: 'hidden',
          borderBottom: '2px solid #f7f7f7',
        }}
      ></PageContainer>
      <div style={{ background: 'white' }}>
        <div className={Style.title}>
          {state < 2 && <div style={{ paddingTop: '20px' }}>填写公司信息</div>}
          {state > 2 && <div style={{ paddingTop: '20px' }}>填写个人信息</div>}
        </div>
        <div>
          <Form labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
            <Form.Item label="选择类型">
              <Radio.Group onChange={onChange} defaultValue={1}>
                <Radio value={1}>公司</Radio>
                <Radio value={3}>个人</Radio>
              </Radio.Group>
            </Form.Item>
          </Form>
        </div>
        {state < 2 && <Company></Company>}
        {state > 2 && <Personal></Personal>}
      </div>

      {/* <Company></Company> */}
      {/* <Personal></Personal> */}
    </div>
  );
}
