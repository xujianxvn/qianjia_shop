import { PageContainer } from '@ant-design/pro-layout';
import React from 'react';
import Style from '@/pages/shop/administration/settled/Renewal/index.less';
import { Button } from 'antd';
import { history } from 'umi';

export default function () {
  const toauthentication = () => {
    history.push('Authentication');
  };
  return (
    <div>
      <PageContainer
        style={{
          height: '20px',
          overflow: 'hidden',
          borderBottom: '2px solid #f7f7f7',
          paddingBottom: '50px',
        }}
      ></PageContainer>
      <h3 style={{ marginLeft: '40px', marginTop: '20px', fontWeight: 'bold' }}>
        申请信息
      </h3>
      <div style={{ display: 'flex', lineHeight: '46px' }}>
        <div style={{ width: '25.3%', textAlign: 'right' }}>开店套餐：</div>123
      </div>
      <div style={{ display: 'flex', lineHeight: '46px' }}>
        <div style={{ width: '25.3%', textAlign: 'right' }}>店铺等级：</div>
        旗舰店铺
      </div>
      <div style={{ display: 'flex', lineHeight: '46px' }}>
        <div style={{ width: '25.3%', textAlign: 'right' }}>续签时长：</div>1年
      </div>
      <div style={{ display: 'flex', lineHeight: '46px' }}>
        <div style={{ width: '25.3%', textAlign: 'right' }}>支付金额：</div>
        5000元
      </div>
      <div style={{ display: 'flex', lineHeight: '46px' }}>
        <div style={{ width: '25.3%', textAlign: 'right' }}>支付凭证：</div>
        <div style={{ width: '200px', height: '200px', textAlign: 'center' }}>
          <img
            style={{ width: '160px', height: '200px' }}
            src="https://img2.baidu.com/it/u=2010444992,1799824962&fm=26&fmt=auto&gp=0.jpg"
            alt=""
          />
        </div>
      </div>
      {/* 审核状态 */}
      <h3 style={{ marginLeft: '40px', marginTop: '20px', fontWeight: 'bold' }}>
        审核状态
      </h3>
      <div style={{ display: 'flex', lineHeight: '46px' }}>
        <div style={{ width: '25.3%', textAlign: 'right' }}>审核状态：</div>
        财务审核
      </div>
      {/* 续签 */}
      <div style={{ background: 'white', paddingTop: 20, marginTop: 20 }}>
        <h3
          style={{ marginLeft: '40px', marginTop: '20px', fontWeight: 'bold' }}
        >
          选择开店套餐
        </h3>
        <div className={Style.content}>
          <div className={Style.content_box}>
            <div className={Style.content_title}>标准店铺</div>
            <div className={Style.content_list}>
              <div>自营专卖店</div>
            </div>
            <div className={Style.content_button}>
              <Button type="primary" onClick={toauthentication}>
                ￥ 2000.00/年
              </Button>
            </div>
          </div>
          <div className={Style.content_box}>
            <div className={Style.content_title}>官方直营店</div>
            <div className={Style.content_list}>
              <div>22222222</div>
            </div>
            <div className={Style.content_button}>
              <Button type="primary">￥ 3000.00/年</Button>
            </div>
          </div>
          <div className={Style.content_box}>
            <div className={Style.content_title}>旗舰店铺</div>
            <div className={Style.content_list}>
              <div>官方旗舰店</div>
            </div>
            <div className={Style.content_button}>
              <Button type="primary">￥ 5000.00/年</Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
