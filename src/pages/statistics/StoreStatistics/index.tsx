import { PageContainer } from '@ant-design/pro-layout';
import './storeStatistics.less';
import React from 'react';
import { Select } from 'antd';
const { Option } = Select;

export default function index() {
  // 实时概况下拉选择
  const contentValue = ['今日实时', '近一周', '近一个月'];
  function handleChange(value: String) {
    console.log(`selected ${value}`);
  }

  // 实时概况内容
  const statisticsData = [
    {
      image:
        'http://uniapp.b2b2c.niuteam.cn/app/shop/view/public/img/stat/add_goods.png',
      title: '新增商品',
    },
    {
      image:
        'http://uniapp.b2b2c.niuteam.cn/app/shop/view/public/img/stat/goods_browse.png',
      title: '商品浏览',
    },
    {
      image:
        'http://uniapp.b2b2c.niuteam.cn/app/shop/view/public/img/stat/goods_collect.png',
      title: '商品收藏',
    },
    {
      image:
        'http://uniapp.b2b2c.niuteam.cn/app/shop/view/public/img/stat/goods_order.png',
      title: '订单商品',
    },
    {
      image:
        'http://uniapp.b2b2c.niuteam.cn/app/shop/view/public/img/stat/shop_collect.png',
      title: '店铺收藏',
    },
    {
      image:
        'http://uniapp.b2b2c.niuteam.cn/app/shop/view/public/img/stat/order_money.png',
      title: '订单金额',
    },
    {
      image:
        'http://uniapp.b2b2c.niuteam.cn/app/shop/view/public/img/stat/order_num.png',
      title: '订单数',
    },
  ];
  return (
    <div>
      <PageContainer
        style={{ height: '50px', overflow: 'hidden' }}
      ></PageContainer>
      {/* 实时概况标题栏 */}
      <div className="display space">
        <div className="border3">实时概况</div>
        <div>
          <Select
            defaultValue={contentValue[0]}
            style={{ width: 120 }}
            loading={false}
            onChange={handleChange}
          >
            {contentValue.map((item) => {
              return (
                <Option value={item} key={item}>
                  {item}
                </Option>
              );
            })}
          </Select>
        </div>
      </div>
      {/* 实时概况内容 */}
      <div className="display wrap contentBox">
        {statisticsData.map((item) => {
          return (
            <div className="display" key={item.title}>
              <img src={item.image} alt="" />
              <div>
                <div>{item.title}</div>
                <div>000</div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
