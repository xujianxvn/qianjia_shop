import { useState } from 'react';
import MyForm from '@/components/MyForm';

const normFile = (e: any) => {
  console.log('Upload event:', e);
  if (Array.isArray(e)) {
    return e;
  }
  return e && e.fileList;
};

export default () => {
  const formConfig = {
    config: {
      name: 'formName',
      labelCol: { span: 6 },
      wrapperCol: { span: 14 },
    },

    formItems: [
      {
        id: 0,
        config: {
          label: 'tinyMCE',
        },
        item: {
          eleType: 'editor',
        },
      },

      {
        id: 1,
        config: {
          label: '文本框',
          name: 'text',
        },
        item: {
          config: {
            placeholder: '请输入文本',
          },
          eleType: 'text',
        },
      },

      {
        id: 2,
        config: {
          label: '密码框',
          name: 'password',
        },
        item: {
          config: {
            placeholder: '请输入密码',
          },
          eleType: 'password',
        },
      },

      {
        id: 3,
        config: {
          label: '数字框',
          name: 'number',
        },
        item: {
          config: {
            min: 1,
            max: 99,
          },
          eleType: 'number',
        },
      },

      {
        id: 4,
        config: {
          label: '多行文本',
          name: 'textarea',
        },
        item: {
          config: {
            placeholder: '请输入内容',
          },
          eleType: 'textarea',
        },
      },

      {
        id: 5,
        config: {
          label: '自动完成',
          name: 'autoComplete',
        },
        item: {
          config: {
            placeholder: '请输入文本',
            options: [
              { label: '', value: '选项1' },
              { label: '', value: '选项2' },
              { label: '', value: '选项3' },
            ],
          },
          eleType: 'autoComplete',
        },
      },

      {
        id: 6,
        config: {
          label: '级联选择',
          name: 'cascader',
        },
        item: {
          config: {
            placeholder: '请选择',
            options: [
              {
                value: '一级',
                label: '一级',
                children: [
                  {
                    value: '二级',
                    label: '二级',
                    children: [{ value: '三级', label: '三级' }],
                  },
                ],
              },
            ],
          },
          eleType: 'cascader',
        },
      },

      {
        id: 7,
        config: {
          label: '选择器',
          name: 'select',
        },
        item: {
          config: {
            placeholder: '请选择',
            mode: 'tags',
            options: [
              { label: '选项1', value: '0' },
              { label: '选项2', value: '1' },
              { label: '选项3', value: '2' },
            ],
          },
          eleType: 'select',
        },
      },

      {
        id: 8,
        config: {
          label: '树选择',
          name: 'treeSelect',
        },
        item: {
          config: {
            treeData: [
              {
                title: '节点1',
                value: '0-0',
                key: '0-0',
                children: [
                  {
                    title: '子节点1-1',
                    value: '0-0-0',
                    key: '0-0-0',
                  },
                  {
                    title: '子节点1-2',
                    value: '0-1-0',
                    key: '0-1-0',
                  },
                ],
              },
              {
                title: '节点2',
                value: '0-1',
                key: '0-1',
                children: [
                  {
                    title: '子节点2-1',
                    value: '0-1-1',
                    key: '0-1-1',
                  },
                  {
                    title: '子节点2-2',
                    value: '0-1-2',
                    key: '0-1-2',
                  },
                ],
              },
            ],
            placeholder: '请选择',
            treeCheckable: true,
          },
          eleType: 'treeSelect',
        },
      },

      {
        id: 9,
        config: {
          label: '日期选择框',
          name: 'datePicker',
        },
        item: {
          config: {
            placeholder: '请选择日期',
            showTime: true,
          },
          eleType: 'datePicker',
        },
      },

      {
        id: 10,
        config: {
          label: '日期范围选择框',
          name: 'rangePicker',
        },
        item: {
          config: {
            showTime: { format: 'HH:mm' },
            format: 'YYYY-MM-DD HH:mm',
          },
          eleType: 'rangePicker',
        },
      },

      {
        id: 11,
        config: {
          label: '多选框组',
          name: 'checkboxGroup',
        },
        item: {
          config: {
            options: ['选项1', '选项2'],
          },
          eleType: 'checkboxGroup',
        },
      },

      {
        id: 12,
        config: {
          label: '单选框组',
          name: 'radioGroup',
        },
        item: {
          config: {
            options: ['选项1', '选项2'],
          },
          eleType: 'radioGroup',
        },
      },

      {
        id: 13,
        config: {
          label: '评分',
          name: 'rate',
        },
        item: {
          config: {
            tooltips: ['terrible', 'bad', 'normal', 'good', 'wonderful'],
          },
          eleType: 'rate',
        },
      },

      {
        id: 14,
        config: {
          label: '开关',
          name: 'switch',
          valuePropName: 'checked',
        },
        item: {
          config: {
            checkedChildren: 'on',
            unCheckedChildren: 'off',
          },
          eleType: 'switch',
        },
      },

      {
        id: 15,
        config: {
          label: '滑动输入条',
          name: 'slider',
        },
        item: {
          config: {
            step: 2,
            range: true,
          },
          eleType: 'slider',
        },
      },

      {
        id: 16,
        config: {
          label: '上传',
          name: 'upload',
          valuePropName: 'fileList',
          getValueFromEvent: normFile,
        },
        item: {
          config: {
            name: 'files',
            listType: 'picture-card',
          },
          eleType: 'upload',
        },
      },

      {
        id: 17,
        config: {
          label: '裁切上传',
          name: 'cropUpload',
          valuePropName: 'fileList',
          getValueFromEvent: normFile,
        },
        item: {
          config: {
            name: 'files',
            listType: 'picture-card',
          },
          eleType: 'imgCropUpload',
        },
      },

      {
        id: 18,
        config: {
          wrapperCol: { offset: 6, span: 14 },
        },
        item: {
          config: {
            type: 'primary',
            htmlType: 'submit',
          },
          eleType: 'button',
          content: '提交',
        },
      },
    ],
  };

  const [editor, setEditor] = useState('');

  const onEditorChange = (newValue: any) => {
    setEditor(newValue);
  };

  const onFinish = (values: any) => {
    values.editor = editor;
    console.log(values);
  };

  const onFinishFailed = (values: any) => {
    console.log(values);
  };

  return (
    <div>
      <MyForm
        formConfig={formConfig}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        onEditorChange={onEditorChange}
      />
    </div>
  );
};
