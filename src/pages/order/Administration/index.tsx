import React, { useEffect } from 'react';
import { DownOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import type { ProColumns } from '@ant-design/pro-table';
import ProTable, { TableDropdown } from '@ant-design/pro-table';
import { Select, Button, Tooltip, Pagination, Card } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import { useState } from 'react';
import { Modal } from 'antd';
import style from './index.less'
import { Table, Tag, Space, Form } from 'antd';
import api from '@/services'
import { EllipsisOutlined } from '@ant-design/icons';
import index from '@/pages/statistics/TransactionStatistics';
import utils from '@/utils'

export default () => {

  const [orderData, setOrderData] = useState([])

  const tableHead = ['商品', '单价/规格/数量', '实付金额', '卖家/收货人', '交易状态', '操作']

  const innerColumns: any = [
    {
      dataIndex: '1',
      width: 200,
      render: (_: any, row: any) => <div style={{display: 'flex', alignItems: 'center'}}>
        <img style={{width: '50px'}} src={row.sku_image} alt="" />
        <div className="text-row-2" style={{width: '150px'}}>{row.sku_name}</div>
      </div>
    },
    {
      dataIndex: '2',
      width: 200,
      render: (_: any, row: any) => <div>
        <div>￥{row.price}</div>
        <div>{row.spec_name}</div>
        <div>{row.num}件</div>
      </div>
    },
    {
      dataIndex: '3',
      width: 200,
      render: (_: any, row: any) => <div>
        ￥{row.goods_money}
      </div>
    },
    {
      dataIndex: '4',
      width: 200,
      render: (_: any, row: any) => <div>
        <div>{row.name}</div>
        <div>{row.mobile}</div>
        <div>{row.address}</div>
      </div>
    },
    {
      dataIndex: '5',
      width: 200,
      render: (_: any, row: any) => <div style={{color: 'red'}}>
        {row.pay_type}
      </div>
    },
    {
      dataIndex: '6',
      width: 200,
    }
  ]
  
  const columns: any = []
  
  tableHead.forEach((item: any, index) => {
    columns.push({
      title: item,
      dataIndex: index,
      width: 200,
      render: (_: any, row: any) => {
        console.log(row);
  
        if (index === 0) {
          return {
            children: (
              <div>
                <div style={{display: 'flex', padding: '5px 15px 15px 0'}}>
                  <span style={{color: 'blue'}}>订单编号：{row.order_no}</span>
                  <span style={{marginLeft: '20px',color: 'blue'}}>下单时间：{utils.formatTime(new Date(row.create_time))}</span>
                  <span style={{margin: '0 15px 0 auto', color: 'red', cursor: 'pointer'}}>查看详情</span>
                  <span style={{color: 'red', cursor: 'pointer'}}>发货</span>
                </div>
                <ProTable
                  rowKey="key"
                  request={() => {
                    row.goodsList.forEach((item: any) => {
                      item.key = item.order_goods_id
                      item.create_time = row.create_time
                      item.address = row.address
                      item.name = row.name
                      item.mobile = row.mobile
                      item.order_status_name = row.order_status_name
                      item.pay_type = row.pay_type
                    });
                    return Promise.resolve({
                      data: row.goodsList,
                      success: true,
                    });
                  }}
                  columns={innerColumns}
                  showHeader={false}
                  toolBarRender={false}
                  pagination={false}
                  search={false}
                />
              </div>
            ),
            props: {
              colSpan: tableHead.length,
            },
          }
        } else {
          return {
            props: {
              colSpan: 0,
            },
          }
        }
      }
    })
  })
  
  //获取订单列表
  const [page, setPage] = useState({ totalRow: 0, totalPage: 0 })
  const getOrderList = async (params: any, sorter: any, filter: any) => {
    const data: any = await api.reqOrderList({ cur_page: params.current })
    console.log(data);
    setOrderData(data.data.data.list)
    setPage(data.data.data.page)
    data.data.data.list.forEach((item: any) => {
      item.key = item.order_id
    })
    console.log(data.data.data.list);

    return {
      data: data.data.data.list,
      success: true
    }
  }

  return (
    <PageContainer>
      <ProTable
        columns={columns}
        request={getOrderList}
        rowKey="key"
        pagination={{
          showQuickJumper: true,
          pageSizeOptions: ['10', '20', '50', '100'],
          total: page.totalRow
        }}
      />
    </PageContainer>
  )
}























// const { Option } = Select;

// //打印
// function dayin() {
//   window.print();
//   window.location.reload();
// }

// function handleChange(value: any) {
//   console.log(`selected ${value}`);
// }
// export type Status = {
//   color: string;
//   text: string;
// };

// export type TableListItem = {
//   key: number;
//   name: string;
//   containers: number;
//   creator: string;
//   status: Status;
//   createdAt: number;
// };
// const tableListDataSource: TableListItem[] = [];

// export default () => {



//   const [visible, setVisible] = useState(false);
//   const [value, setvalue] = useState<any>({})

//   const renderContent = (value: any, row: any, index: any) => ({
//     children: value,
//     props: { colSpan: 0 },
//   });

//   const columns: ProColumns<TableListItem>[] = [
//     {
//       title: '',
//       width: '100%',
//       dataIndex: 'order_name',
//       render: (_, row: any) => ({
//         children: (
//           <>
//             <div style={{ display: 'flex' }}>
//               <span>{`订单号:${row.order_no}`}</span>
//               <span style={{ margin: '0 20px' }}>{`下单时间:${row.finish_time}`}</span>
//               <span>{`支付方式:${row.pay_type}`}</span>
//               <span style={{ marginLeft: 'auto ' }}>
//                 <a onClick={() => {
//                   row.goods_list.forEach((item: any) => {
//                     item.key = item.order_goods_id
//                     item.name = row.name
//                     item.mobile = row.mobile
//                     item.address = row.address
//                     item.order = row.order_status_name
//                   })
//                   setvalue(row);
//                   setVisible(true)
//                 }} style={{ margin: '0 20px' }}>查看详情</a>
//                 <a>备注</a>
//               </span>
//             </div>
//           </>
//         ),
//         props: {
//           colSpan: 8,
//         },
//       })
//     },

//   ];
//   const expandColumns: ProColumns<TableListItem>[] = [
//     {
//       title: '商品',
//       width: 1,
//       dataIndex: 'sku_name',
//       key: '1',
//       align:'center'
//     },
//     {
//       title:'图片',
//       width:100,
//       dataIndex:'sku_image',
//       key:'7',
//       render:(sku_image)=>{
//         return <img src={`${sku_image}`} alt="" style={{width:"80px",height:'80px'}}/>
//       }
//     },
//     {
//       title: '数量',
//       width: 120,
//       dataIndex: 'num',
//       align: 'center',
//       sorter: (a, b) => a.containers - b.containers,


//     },
//     {
//       title: '维权',
//       width: 120,
//       dataIndex: 'containers',
//       align:'center'
//     },

//     {
//       title: '实付金额',
//       width: 120,
//       dataIndex: 'price',
//       render:(price)=> {
//         return <div style={{color:'red'}}>{`￥${price}`}</div>
//       },
//       align:'center'
//     },
//     {
//       title: '买家/收货人',
//       width: 120,
//       dataIndex: 'name',
//       align:'center'
//     },
//     {
//       title: '交易状态',
//       width: 120,
//       dataIndex: 'order',
//       align:'center'
//     },

//   ]


//   const expandedRowRender = (row: any) => {
//     // console.log(row);
//     row.goods_list.forEach((item: any) => {
//       item.key = item.order_goods_id
//       item.name = row.name
//       item.mobile = row.mobile
//       item.address = row.address
//       item.order = row.order_status_name
//     })

//     return (
//       <ProTable
//         columns={expandColumns}
//         headerTitle={false}
//         search={false}
//         options={false}
//         dataSource={row.goods_list}
//         pagination={false}
//       />
//     );
//   };

//   const attr = async () => {
//     const data: any = await api.reqOrderList()
//     // console.log(data);
//     data.data.data.forEach((item: any) => {
//       item.key = item.order_id
//     })
//     return Promise.resolve({
//       data: data.data.data,
//       success: true,
//       list: data.data
//     });
//   }

//   useEffect(() => {

//     attr()

//   }, [])



//   return (


//     <div>
//       <PageContainer style={{ height: '50px', overflow: 'hidden' }}></PageContainer>

//       <Form
//         layout="inline"
//         onFinish={(values) => {
//           console.log(values);

//         }}
//       >
//         <Form.Item
//           name="order_type"
//           label='订单类型'
//         >
//           <Select placeholder="请选择" style={{ width: 150 }}>
//             <Option value="0">全部</Option>
//             <Option value="1">普通订单</Option>
//             <Option value="2">自提订单</Option>
//             <Option value="3">外卖订单</Option>
//             <Option value="4">虚拟订单</Option>
//           </Select>
//         </Form.Item>
//         <Form.Item
//           name="order_status"
//           label='订单状态'
//         >
//         <Select placeholder="请选择" style={{ width: 150 }}>
//             <Option value="0">全部</Option>
//             <Option value="1">待支付</Option>
//             <Option value="2">待发货</Option>
//             <Option value="3">已发货</Option>
//             <Option value="4">已收货</Option>
//           </Select>
//         </Form.Item>
//         <Form.Item>
//           <Button type="primary" htmlType="submit">提交</Button>
//         </Form.Item>
//       </Form>



//       <ProTable<TableListItem>
//         columns={columns}
//         request={attr}
//         pagination={{
//           showQuickJumper: true,
//         }}
//         expandable={{ expandedRowRender }}
//         search={false}
//         dateFormatter="string"
//         options={false}

//       />


//       <Modal
//         title="订单详情"
//         centered
//         visible={visible}
//         onOk={() => setVisible(false)}
//         onCancel={() => setVisible(false)}
//         width={1400}
//       >
//         <div style={{ display: 'flex', width: '100%' }}>
//           <div style={{ width: '48%', padding: '10px 15px', borderRight: '1px solid #f0f0f0' }}>
//             <h2>订单信息</h2>
//             <div className={style.a}>
//               <span>订单编号:</span>
//               <span>{value.order_no}</span>
//             </div>
//             <div className={style.a}>
//               <span>订单类型:</span>
//               <span>{value.order_type}</span>
//             </div>
//             <div className={style.a}>
//               <span>订单来源:</span>
//               <span>{value.order_from_name}</span>
//             </div>
//             <div className={style.a}>
//               <span>买家:</span>
//               <span>{value.mobile}</span>
//             </div>
//             <div className={style.b}>
//               <span>配送方式:</span>
//               <span>物流配送</span>
//             </div>
//             <div className={style.a}>
//               <span>收货人:</span>
//               <span>{value.name}</span>
//             </div>
//             <div className={style.a}>
//               <span>联系电话:</span>
//               <span>{value.mobile}</span>
//             </div>
//             <div className={style.a}>
//               <span>收货地址:</span>
//               <span>{value.address}</span>
//             </div>
//           </div>
//           <div style={{ marginLeft: '3%' }}>
//             <h2>订单状态:已关闭</h2>
//             <div>
//               <Button style={{ backgroundColor: '#FF6A00', color: 'white' }} type="link" onClick={dayin} className={style.c}>
//                 打印
//               </Button>
//               <Button style={{ backgroundColor: '#FF6A00', color: 'white' }} type="link" className={style.c}>
//                 删除
//               </Button>
//               <Button style={{ backgroundColor: '#FF6A00', color: 'white' }} type="link" className={style.c}>
//                 修改地址
//               </Button>
//             </div>
//             <div style={{ marginTop: 120 }}>
//               <Card title="提醒:" bordered={false} style={{ color: '#999 !important', fontSize: 14 }} headStyle={{ color: '#ff8143' }}>
//                 <div>交易成功后，平台将把货款结算至你的店铺账户余额，您可以申请提现；</div>
//                 <div>请及时关注你发出的包裹状态，确保能配送至买家手中；</div>
//                 <div>如果买家表示未收到货或者货物有问题，请及时联系买家积极处理，友好协商；</div>
//               </Card>
//             </div>,
//           </div>
//         </div>
//         <div style={{ marginTop: 20, borderTop: '1px solid #f0f0f0', paddingTop: 20 }}>
//           {/* <Table columns={columnss} /> */}
//         </div>
//       </Modal>
//     </div>
//   );
// };

