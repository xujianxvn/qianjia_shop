import React, { useEffect } from 'react';
import { DownOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import type { ProColumns } from '@ant-design/pro-table';
import ProTable, { TableDropdown } from '@ant-design/pro-table';
import { Select, Button, Tooltip, Pagination, Card, Form, Input } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import { useState } from 'react';
import { Modal } from 'antd';
import style from './index.less'
import { Table, Tag, Space } from 'antd';
import api from '@/services'
import { EllipsisOutlined } from '@ant-design/icons';
import { render } from 'react-dom';
import { isTemplateElement } from '@babel/types';



const { Option } = Select;

//打印
function dayin() {
  window.print();
  window.location.reload();
}


function handleChange(value: any) {
  console.log(`selected ${value}`);
}
export type Status = {
  color: string;
  text: string;
};

export type TableListItem = {
  key: number;
  name: string;
  containers: number;
  creator: string;
  status: Status;
  createdAt: number;
};
const tableListDataSource: TableListItem[] = [];

export default () => {



  const [visible, setVisible] = useState(false);
  const [value, setvalue] = useState<any>({})
  const [lists, setlists] = useState<any>({})
  const [ddd, setddd] = useState<any>([])

  // console.log(value);

  const columns: ProColumns<TableListItem>[] = [
    {
      title: '',
      width: '100%',
      dataIndex: 'order_name',
      render: (_, row: any) => ({
        children: (
          <>
            <div style={{ display: 'flex' }}>
              <span>{`订单号:${row.order_no}`}</span>
              <span style={{ margin: '0 20px' }}>{`下单时间:${row.finish_time}`}</span>
              <span>{`支付方式:${row.pay_type}`}</span>
              <span style={{ marginLeft: 'auto ' }}>
                <a onClick={() => {
                  row.goods_list.forEach(async (item: any) => {
                    item.key = item.order_goods_id
                    item.name = row.name
                    item.mobile = row.mobile
                    item.address = row.address
                    item.order = row.order_status_name
                  })

                  let dd = lists.filter((item: any) => item.order_id === row.order_id)
                  console.log(dd[0].goods_list);
                  setddd(dd[0].goods_list)
                  console.log(ddd)
                  setvalue(row);
                  setVisible(true)
                }} style={{ margin: '0 20px' }} >查看详情</a>
                <a>备注</a>
              </span>
            </div>
          </>
        ),
        props: {
          colSpan: 8,
        },
      })
    },

  ];
  const expandColumns: ProColumns<TableListItem>[] = [
    {
      title: '商品信息',
      width: 1,
      dataIndex: 'sku_name',
      key: '1'
    },
    {
      title: '图片',
      width: 1,
      dataIndex: 'sku_image',
      key: '7',
      render: (sku_image) => {
        return <img src={`${sku_image}`} alt="" style={{ width: "80px", height: '80px' }} />
      }
    },
    {
      title: '发货状态',
      width: 120,
      dataIndex: 'delivery_status_name',
      align: 'center',
      sorter: (a, b) => a.containers - b.containers,

    },
    {
      title: '订单金额',
      width: 120,
      dataIndex: 'price',
      align: 'center',
      render: (price) => {
        return <div style={{ color: 'red' }}>{`￥${price}`}</div>
      }
    },

    {
      title: '退款金额',
      width: 120,
      dataIndex: 'price',
      align: 'center',
      render: (price) => {
        return <div style={{ color: 'red' }}>{`￥${price}`}</div>
      }
    },
    {
      title: '退款状态',
      width: 120,
      dataIndex: 'refund_status_name',
      align: 'center'
    },
  ]


  const expandedRowRender = (row: any) => {
    console.log(row);
    row.goods_list.forEach((item: any) => {
      item.key = item.order_goods_id
      item.name = row.name
      item.mobile = row.mobile
      item.address = row.address
      item.order = row.order_status_name
    })

    return (
      <ProTable
        columns={expandColumns}
        headerTitle={false}
        search={false}
        options={false}
        dataSource={row.goods_list}
        pagination={false}
      />
    );
  };



  const attr = async () => {
    const data: any = await api.reqOrderList()
    // console.log(data);
    data.data.data.forEach((item: any) => {
      item.key = item.order_id
    })
    setlists(data.data.data)
    return Promise.resolve({
      data: data.data.data,
      success: true,
      list: data.data
    });
  }



  useEffect(() => {

    attr()

  }, [])

  return (
    <div>
      <PageContainer style={{ height: '50px', overflow: 'hidden' }}></PageContainer>
      <Form
        layout="inline"
        onFinish={(values) => {
          console.log(values);

        }}
      >
        <Form.Item
          name="order_type"
          label='商品名称'
        >
          <Input
            type="text"
            style={{ width: 140 }}
          />
        </Form.Item>
        <Form.Item
          name="order_status"
          label='订单编号'
        >
          <Input
            type="text"
            style={{ width: 140 }}
          />
        </Form.Item>
        <Form.Item
          name="order_status"
          label='退款编号'
        >
          <Input
            type="text"
            style={{ width: 140 }}
          />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">提交</Button>
        </Form.Item>
      </Form>

      <ProTable<TableListItem>
        columns={columns}
        request={attr}
        pagination={{
          showQuickJumper: true,
        }}
        expandable={{ expandedRowRender }}
        search={false}
        dateFormatter="string"
        options={false}

      />

      <Modal
        title="订单详情"
        centered
        visible={visible}
        onOk={() => setVisible(false)}
        onCancel={() => setVisible(false)}
        width={1400}
      >
        <div style={{ display: 'flex', width: '100%' }} >
          <div style={{ width: '48%', padding: '10px 15px', borderRight: '1px solid #f0f0f0' }}>
            <h2>订单信息</h2>
            <div className={style.a}>
              <span>订单编号:</span>
              <span>{value.order_no}</span>
            </div>
            <div className={style.a}>
              <span>订单类型:</span>
              <span>{value.order_type}</span>
            </div>
            <div className={style.a}>
              <span>订单来源:</span>
              <span>{value.order_from_name}</span>
            </div>
            <div className={style.a}>
              <span>买家:</span>
              <span>{value.mobile}</span>
            </div>
            <div className={style.b}>
              <span>配送方式:</span>
              <span>物流配送</span>
            </div>
            <div className={style.a}>
              <span>收货人:</span>
              <span>{value.name}</span>
            </div>
            <div className={style.a}>
              <span>联系电话:</span>
              <span>{value.mobile}</span>
            </div>
            <div className={style.a}>
              <span>收货地址:</span>
              <span>{value.address}</span>
            </div>
          </div>
          <div style={{ marginLeft: '3%' }}>
            <h2 style={{ color: 'red' }}>退款状态:已退款</h2>
            <div>
              <Button style={{ backgroundColor: '#FF6A00', color: 'white' }} type="link" onClick={dayin} className={style.c}>
                打印
              </Button>
            </div>
            <div style={{ marginTop: 120 }}>
              <Card title="提醒:" bordered={false} style={{ color: '#999 !important', fontSize: 14 }} headStyle={{ color: '#ff8143' }}>
                <div>交易成功后，平台将把货款结算至你的店铺账户余额，您可以申请提现；</div>
                <div>请及时关注你发出的包裹状态，确保能配送至买家手中；</div>
                <div>如果买家表示未收到货或者货物有问题，请及时联系买家积极处理，友好协商；</div>
              </Card>
            </div>,
          </div>
        </div>
        <div style={{ marginTop: 20, borderTop: '1px solid #f0f0f0', paddingTop: 20, width: '80%', display: 'flex', justifyContent: 'space-between' }}>
          <div style={{ display: 'flex', width: '30%' }} >
            {ddd.map((item: any, index: any) => (
              <div key={index + ''} style={{ display: 'flex' }}>
                <img src={item.sku_image} alt="" style={{ width: "80px", height: '80px', margin: 20 }} />
                <div style={{ margin: 20 }}>{item.sku_name}</div>
              </div>
            ))}
          </div>
          <div>
            <h5 style={{ fontWeight: 900 }}>售后信息</h5>
            <div className={style.d}>
              <div>退款方式:</div>
              <div className={style.e}>仅退款</div>
            </div>
            <div className={style.d}>
              <div>退款金额:</div>
              <div className={style.e}>￥{value.price}</div>
            </div>
            <div className={style.d}>
              <div>联系方式:</div>
              <div style={{ marginLeft: 5 }}>{value.mobile}</div>
            </div>
            <div className={style.d}>
              <div>退款原因:</div>
              <div style={{ marginLeft: 5 }}>拍错/多拍/不喜欢</div>
            </div>
            <div className={style.d}>
              <div>退款说明:</div>
              <div style={{ marginLeft: 5 }}>11111</div>
            </div>
          </div>
          <div>
            <h5 style={{ fontWeight: 900 }}>购买信息</h5>
            {ddd.map((item: any, index: any) => (
              <div key={index + ''} >
                <div className={style.d}>
                  <div>退款数量:</div>
                  <div className={style.e}>{value.price}</div>
                  <div style={{ marginLeft: 5 }}>x{item.num}件</div>
                </div>
                <div className={style.d}>
                  <div>实付金额:</div>
                  <div className={style.e}>￥{item.price}</div>
                </div>
              </div>
            ))}

            <div className={style.d}>
              <div>配送状态:</div>
              <div style={{ marginLeft: 5 }}>未发货</div>
            </div>
            <div className={style.d}>
             
              {ddd.map((item: any, index: any) => (
                <div key={index + ''} style={{display:'flex'}}>
                   <div>订单编号:</div>
                  <div className={style.e}> {item.order_no}</div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </Modal>
    </div>
  );
};

