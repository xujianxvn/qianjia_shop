import { Row, Col, Divider, Table, Card } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';
import React, { useState, useEffect } from 'react';
import { Column, ColumnConfig } from '@ant-design/charts';
import api from '@/services';

export default function index() {
  // 会员列表
  const columns = [
    {
      title: '账户',
      dataIndex: 'nickname',
    },
    {
      title: '会员等级',
      dataIndex: 'member_level',
    },
    {
      title: '积分',
      dataIndex: 'point',
    },
    {
      title: '创建时间',
      dataIndex: 'reg_time',
    },
  ];
  const [data1, setData1] = useState([]);
  const [memberInfo, setmemberInfo] = useState([]);

  const getmemberCount = async () => {
    const list: any = await api.reqmemberCount();
    console.log(list.data.data);
    setmemberInfo(list.data.data);
    list.data.data.map((item: any) => {
      item.key = item.member_id;
      // item.reg_time = item.reg_time.
    });
    setData1(list.data.data);
    console.log(list.data.data);
  };
  useEffect(() => {
    getmemberCount();
  }, []);

  var data = [
    {
      type: '累计会员数',
      sales: memberInfo.length,
    },
    {
      type: '今日新增会员数',
      sales: 52,
    },
    {
      type: '关注会员数',
      sales: 61,
    },
    {
      type: '下单会员数',
      sales: 145,
    },
  ];
  var config = {
    data: data,
    xField: 'type',
    yField: 'sales',
    label: {
      // position: 'middle',
      // style: {
      //   fill: '#FFFFFF',
      //   opacity: 0.6,
      // },
    },
    xAxis: {
      label: {
        autoHide: true,
        autoRotate: false,
      },
    },
    meta: {
      type: { alias: '类别' },
      sales: { alias: '人数' },
    },
  };

  function onChange(pagination: any, filters: any, sorter: any, extra: any) {
    console.log('params', pagination, filters, sorter, extra);
  }

  return (
    <div>
      <PageContainer
        style={{
          height: '40px',
          overflow: 'hidden',
          borderBottom: '2px solid #f7f7f7',
          paddingBottom: '50px',
        }}
      ></PageContainer>
      <Row gutter={16} style={{ marginTop: '20px' }}>
        <Col className="gutter-row" span={6}>
          <Card>
            <div style={{ fontSize: '30px', color: 'red' }}>
              {memberInfo.length}
            </div>
            <div style={{ fontSize: '16px', color: '#666666' }}>累计会员数</div>
          </Card>
        </Col>
        <Col className="gutter-row" span={6}>
          <Card>
            <div style={{ fontSize: '30px', color: 'red' }}>65</div>
            <div style={{ fontSize: '16px', color: '#666666' }}>
              今日新增会员数
            </div>
          </Card>
        </Col>
        <Col className="gutter-row" span={6}>
          <Card>
            <div style={{ fontSize: '30px', color: 'red' }}>65</div>
            <div style={{ fontSize: '16px', color: '#666666' }}>关注会员数</div>
          </Card>
        </Col>
        <Col className="gutter-row" span={6}>
          <Card>
            <div style={{ fontSize: '30px', color: 'red' }}>65</div>
            <div style={{ fontSize: '16px', color: '#666666' }}>下单会员数</div>
          </Card>
        </Col>
      </Row>
      <Row style={{ marginTop: '20px' }}>
        <Col flex="500px">
          <Card>
            <Column {...config} />
          </Card>
        </Col>
        <Col flex="auto">
          <Card style={{ marginLeft: '20px' }}>
            <Table columns={columns} dataSource={data1} onChange={onChange} />
          </Card>
        </Col>
      </Row>
    </div>
  );
}
function key(key: any, index: any) {
  throw new Error('Function not implemented.');
}
