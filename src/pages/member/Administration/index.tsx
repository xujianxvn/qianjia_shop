import { PageContainer } from '@ant-design/pro-layout';
import React, { useState } from 'react';
import {
  Button,
  Modal,
  Collapse,
  Tooltip,
  Dropdown,
  Menu,
  Input,
  Image,
  Table,
} from 'antd';
import {
  EllipsisOutlined,
  QuestionCircleOutlined,
  SearchOutlined,
} from '@ant-design/icons';
import type { ProColumns } from '@ant-design/pro-table';
import ProTable, { TableDropdown } from '@ant-design/pro-table';
import api from '@/services';

const { Panel } = Collapse;
const valueEnum = {
  0: 'close',
  1: 'running',
  2: 'online',
  3: 'error',
};

export type TableListItem = {
  is_auth: number;
  key: any;
  member_id: number;
};

export default function index() {
  // 详情对话框
  const [visible, setVisible] = useState(false);
  // 明细对话框
  const [visible1, setVisible1] = useState(false);

  const [rowvalue, setrowvalue] = useState<any>({});

  const columns: ProColumns<TableListItem>[] = [
    {
      key: 'index',
      title: '排序',
      dataIndex: 'index',
      valueType: 'indexBorder',
      width: 48,
    },
    {
      key: 'nickname',
      title: '昵称',
      dataIndex: 'nickname',
    },
    {
      key: 'mobile',
      title: '手机号',
      dataIndex: 'mobile',
    },
    {
      key: 'email',
      title: '邮箱',
      dataIndex: 'email',
    },
    {
      key: 'is_auth',
      title: '是否认证',
      dataIndex: 'is_auth',
      initialValue: 'all',
      filters: true,
      onFilter: true,
      search: false,
      valueEnum: {
        all: { text: '全部', status: 'Default' },
        close: { text: '是', status: 'Default' },
        running: { text: '否', status: 'Processing' },
      },
      render: (_, row) => {
        return row.is_auth ? '是' : '否';
      },
    },
    {
      title: '关注时间',
      dataIndex: 'reg_time',
      width: 140,
      key: 'reg_time',
      valueType: 'date',
      sorter: true,
      hideInSearch: true,
    },
    {
      title: '关注时间',
      dataIndex: 'reg_time',
      valueType: 'dateRange',
      hideInTable: true,
      search: {
        transform: (value) => {
          return {
            startTime: value[0],
            endTime: value[1],
          };
        },
      },
    },
    {
      key: 'memo',
      title: '备注',
      dataIndex: 'memo',
      ellipsis: true,
      copyable: true,
      search: false,
    },
    {
      title: '操作',
      width: 180,
      key: 'option',
      valueType: 'option',
      render: (_, row) => [
        <a
          key="link"
          onClick={() => {
            setVisible(true), setrowvalue(row);
          }}
        >
          详情
        </a>,
        <a
          key="link2"
          onClick={async () => {
            setVisible1(true);
            const res: any = await api.reorderlist({ id: row.member_id });
            res.data.data.map((item: any) => {
              item.key = item.member_id;
            });
            setdata(res.data.data);
          }}
        >
          账户明细
        </a>,
      ],
    },
  ];

  const detailsColumns = [
    {
      title: '订单编号',
      dataIndex: 'order_no',
    },
    {
      title: '店铺名称',
      dataIndex: 'site_name',
    },
    {
      title: '商品信息',
      dataIndex: 'order_name',
    },
    {
      title: '订单金额 ',
      dataIndex: 'goods_money',
    },
    {
      title: '实际支付金额',
      dataIndex: 'goods_money',
    },
    {
      title: '订单状态',
      dataIndex: 'order_status',
    },
    {
      title: '创建时间',
      dataIndex: 'pay_time',
    },
  ];
  const [data, setdata] = useState([]);

  return (
    <div>
      <PageContainer
        style={{ height: '50px', overflow: 'hidden' }}
      ></PageContainer>
      <Collapse
        defaultActiveKey={['1']}
        // onChange={callback}
        style={{ marginTop: 20 }}
      >
        <Panel header="操作提示" key="1">
          <ul style={{ color: 'gray' }}>
            <li style={{ lineHeight: 3 }}>
              会员由平台端进行统一管理，平台可以针对会员进行添加，编辑，锁定，调整账户等操作。
            </li>
            <li style={{ lineHeight: 3 }}>
              账户类型有用户名、手机、邮箱三种类型，筛选时可以选择其中一种类型并输入对应的内容进行筛选。
            </li>
            <li style={{ lineHeight: 3 }}>
              点击收起按钮可以将搜索面板隐收起，变成高级搜索按钮。
            </li>
          </ul>
        </Panel>
      </Collapse>
      <ProTable<TableListItem>
        columns={columns}
        request={async (params) => {
          const tableListDataSource: any = await api.reqmembeQuery({
            value: params,
          });
          tableListDataSource.data.data.forEach((item: any) => {
            item.key = item.member_id;
          });
          return {
            data: tableListDataSource.data.data,
            success: true,
          };
        }}
        // rowKey="1"
        pagination={{
          showQuickJumper: true,
        }}
        search={{
          layout: 'vertical',
          defaultCollapsed: true,
        }}
        dateFormatter="string"
        toolbar={{
          title: '会员列表',
          tooltip: '这是一个关于会员的列表',
        }}
      />
      {/* 详情对话框 */}
      <Modal
        title="会员详情"
        centered
        visible={visible}
        onOk={() => setVisible(false)}
        onCancel={() => setVisible(false)}
        width={1000}
      >
        <div>
          <div style={{ fontSize: '14px', fontWeight: 800 }}>基础信息</div>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-around',
              fontSize: '14px',
              color: '#454545',
              marginTop: '30px',
            }}
          >
            <div style={{ display: 'flex' }}>
              <Image
                width={50}
                src="https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png"
              />
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'space-between',
                  marginLeft: '10px',
                }}
              >
                <div>
                  会员昵称：<span>{rowvalue.nickname}</span>
                </div>
                <div>
                  关注状态：{rowvalue.is_auth < 1 && <span>未关注</span>}
                  {rowvalue.is_auth > 0 && <span>关注</span>}
                </div>
              </div>
            </div>

            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-between',
              }}
            >
              <div>
                手机电话：<span>{rowvalue.mobile}</span>
              </div>
              <div>
                关注时间：<span>{rowvalue.reg_time}</span>
              </div>
            </div>

            <div>
              会员邮箱: <span>{rowvalue.email}</span>
            </div>
          </div>
        </div>
      </Modal>
      {/* 明细对话框 */}
      <Modal
        title="账户明细"
        centered
        visible={visible1}
        onOk={() => setVisible1(false)}
        onCancel={() => setVisible1(false)}
        width={1000}
      >
        <Table columns={detailsColumns} dataSource={data} key="id" />,
      </Modal>
    </div>
  );
}
