import { PageContainer } from '@ant-design/pro-layout';
import { Collapse, Button, Input } from 'antd';
import MyForm from '@/components/MyForm';
import React, { useState, useEffect } from 'react';
import Style from '@/pages/assets/storeSettlement/StoreSettlement/index.less';
import { Table, Tag, Space } from 'antd';
import { history } from 'umi';
import api from '@/services';
import dayjs from 'dayjs';

const { Panel } = Collapse;
function callback() {}

export default function index() {
  // 表单
  const formConfig = {
    config: {
      name: 'formName',
      labelCol: { span: 30 },
      wrapperCol: { span: 30 },
    },
    formItems: [
      {
        id: 10,
        config: {
          label: '',
          name: 'rangePicker',
        },
        item: {
          config: {
            showTime: { format: 'HH:mm' },
            format: 'YYYY-MM-DD HH:mm',
          },
          eleType: 'rangePicker',
        },
      },
    ],
  };

  // 表单
  const [editor, setEditor] = useState('');

  const onEditorChange = (newValue: any) => {
    setEditor(newValue);
  };

  const onFinish = (values: any) => {
    values.editor = editor;
    console.log(values);
  };

  const onFinishFailed = (values: any) => {
    console.log(values);
  };

  //数据表格
  const columns = [
    {
      title: '流水号',
      dataIndex: 'settlement_no',
      key: 'settlement_no',
    },
    {
      title: '订单总额',
      dataIndex: 'order_money',
      key: 'order_money',
    },
    {
      title: '店铺金额',
      dataIndex: 'shop_money',
      key: 'shop_money',
    },
    {
      title: '平台抽成',
      dataIndex: 'platform_money',
      key: 'platform_money',
    },
    {
      title: '佣金金额',
      dataIndex: 'commission',
      key: 'commission',
    },
    {
      title: '账期开始时间',
      dataIndex: 'start_time',
      key: 'startTime',
    },
    {
      title: '账期结束时间',
      dataIndex: 'end_time',
      key: 'endTime',
    },
    {
      title: '操作',
      key: 'action',
      render: (text: any) => (
        <Space size="middle">
          <a onClick={toDetail}>详情</a>
        </Space>
      ),
    },
  ];

  //点击详情，跳转详情页
  const toDetail = () => {
    history.push('/assets/storeSettlement/StoreDetail');
  };

  //获取数据列表
  const [data, setdata] = useState([]);
  const getList = async () => {
    const res: any = await api.reqLookDetail();
    console.log(res);
    const list = res.data.data.map((item: any) => {
      return {
        key: item.id.toString(),
        settlement_no: item.settlement_no,
        order_money: item.order_money.toFixed(2),
        shop_money: item.shop_money.toFixed(2),
        platform_money: item.platform_money.toFixed(2),
        commission: item.commission.toFixed(2),
        start_time: dayjs(item.start_time).format('YYYY-MM-DD HH:MM:ss'),
        end_time: dayjs(item.end_time).format('YYYY-MM-DD HH:MM:ss'),
      };
    });
    setdata(list);
  };

  //生命周期
  useEffect(() => {
    getList();
  }, []);

  return (
    <div>
      {/* 提示 */}
      <PageContainer
        style={{ height: '50px', overflow: 'hidden' }}
      ></PageContainer>
      <Collapse
        defaultActiveKey={['1']}
        onChange={callback}
        style={{ marginTop: 20 }}
      >
        <Panel header="操作提示" key="1">
          <ul style={{ color: 'gray' }}>
            <li style={{ lineHeight: 3 }}>
              平台设置店铺结算周期后，系统会按照设置定期进行结算。
            </li>
            <li style={{ lineHeight: 3 }}>
              结算周期分为按天、按周、按月。按天是指每天0点开始，按周是指每周一0点开始，按月是指每月一号0点开始。
            </li>
          </ul>
        </Panel>
      </Collapse>

      {/* 时间表单 */}
      <div
        style={{
          display: 'flex',
          marginLeft: '900px',
          marginTop: '20px',
          marginBottom: '20px',
        }}
      >
        <div>
          <MyForm
            formConfig={formConfig}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            onEditorChange={onEditorChange}
          />
        </div>
        <div className={Style.stosettle_search}>搜索</div>
      </div>

      {/* 数据表格 */}
      <div>
        <Table columns={columns} dataSource={data} />
      </div>
    </div>
  );
}
