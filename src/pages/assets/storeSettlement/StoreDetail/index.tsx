import { PageContainer } from '@ant-design/pro-layout';
import { Collapse, Button, Input } from 'antd';
import MyForm from '@/components/MyForm';
import React, { useState, useEffect } from 'react';
import Style from '@/pages/assets/storeSettlement/StoreDetail/index.less';
import { Table, Tag, Space } from 'antd';
import { history } from 'umi';
import api from '@/services';

const { Panel } = Collapse;
function callback() {}

export default function index() {
  //数据表格
  const columns = [
    {
      title: '订单编号',
      dataIndex: 'order_no',
      key: 'order_no',
    },
    {
      title: '订单销售额',
      dataIndex: 'order_money',
      key: 'order_money',
    },
    {
      title: '订单退款',
      dataIndex: 'refund_money',
      key: 'refund_money',
    },
    {
      title: '佣金',
      dataIndex: 'commission',
      key: 'commission',
    },
    {
      title: '平台优惠券',
      dataIndex: 'coupon_money',
      key: 'coupon_money',
    },
    {
      title: '平台优惠券（退款部分）',
      dataIndex: 'promotion_money',
      key: 'promotion_money',
    },
    {
      title: '店铺收入',
      dataIndex: 'pay_money',
      key: 'pay_money',
    },
  ];

  //获取订单信息
  const [data, setdata] = useState([]);
  const [income, setIncome] = useState(0);
  const [refund, setRefund] = useState(0);
  const [commission, setCommission] = useState(0);
  const [coupon, setCoupon] = useState(0);
  const [promotion, setPromotion] = useState(0);
  const getOrder = async () => {
    const res: any = await api.reqStoreSettle();
    const list = res.data.data.map((item: any) => {
      return {
        key: item.order_id.toString(),
        order_no: item.order_no,
        order_money: item.order_money.toFixed(2),
        refund_money: item.refund_money.toFixed(2),
        commission: item.commission.toFixed(2),
        coupon_money: item.coupon_money.toFixed(2),
        promotion_money: item.promotion_money.toFixed(2),
        pay_money: item.pay_money.toFixed(2),
      };
    });
    setdata(list);

    //店铺收入金额
    const totalIncome = list.reduce((pre: any, item: any) => {
      return pre + Number(item.pay_money);
    }, 0);
    setIncome(totalIncome.toFixed(2));

    //店铺退款金额
    const totalRefund = list.reduce((pre: any, item: any) => {
      return pre + Number(item.refund_money);
    }, 0);
    setRefund(totalRefund.toFixed(2));

    //佣金金额
    const totalCommission = list.reduce((pre: any, item: any) => {
      return pre + Number(item.commission);
    }, 0);
    setCommission(totalCommission.toFixed(2));

    //平台优惠券平台承担金额
    const totalCoupon = list.reduce((pre: any, item: any) => {
      return pre + Number(item.coupon_money);
    }, 0);
    setCoupon(totalCoupon.toFixed(2));

    //平台优惠券平台承担金额
    const totalPromotion = list.reduce((pre: any, item: any) => {
      return pre + Number(item.promotion_money);
    }, 0);
    setPromotion(totalPromotion.toFixed(2));
  };

  //生命周期
  useEffect(() => {
    getOrder();
  }, []);

  return (
    <div>
      {/* 提示 */}
      <PageContainer
        style={{ height: '50px', overflow: 'hidden' }}
      ></PageContainer>
      <Collapse
        defaultActiveKey={['1']}
        onChange={callback}
        style={{ marginTop: 20 }}
      >
        <Panel header="操作提示" key="1">
          <ul style={{ color: 'gray' }}>
            <li style={{ lineHeight: 3 }}>
              店铺收入金额 = 店铺结算总金额 - 店铺退款金额 - 佣金金额 +
              平台优惠券平台承担金额 - 平台优惠券平台承担金额（退款部分）。
            </li>
            <li style={{ lineHeight: 3 }}>
              平台抽成金额 = 平台结算总抽成 - 平台退款抽成。
            </li>
          </ul>
        </Panel>
      </Collapse>

      {/* 店铺结算 */}
      <div
        style={{
          borderLeft: '3px solid #1890FF',
          fontWeight: 'bold',
          marginLeft: '20px',
          paddingLeft: '10px',
          marginTop: '30px',
        }}
      >
        店铺结算
      </div>
      <div
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          paddingLeft: '50px',
          paddingRight: '50px',
          paddingTop: '35px',
          color: '#000000',
          marginBottom: '30px',
        }}
      >
        <div className={Style.storede_content1}>
          <div>店铺收入金额（元）</div>
          <div className={Style.storede_profit}>{income}</div>
        </div>
        <div className={Style.storede_content1}>
          <div>店铺退款金额（元）</div>
          <div className={Style.storede_profit}>{refund}</div>
        </div>
        <div className={Style.storede_content1}>
          <div>佣金金额（元）</div>
          <div className={Style.storede_profit}>{commission}</div>
        </div>
        <div className={Style.storede_content1}>
          <div>平台优惠券平台承担金额（元）</div>
          <div className={Style.storede_profit}>{coupon}</div>
        </div>
        <div className={Style.storede_content1}>
          <div>平台优惠券平台承担金额（退款部分）（元）</div>
          <div className={Style.storede_profit}>{promotion}</div>
        </div>
      </div>

      {/* 数据表格 */}
      <div>
        <Table columns={columns} dataSource={data} />
      </div>
    </div>
  );
}
