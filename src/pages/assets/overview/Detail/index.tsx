import { PageContainer } from '@ant-design/pro-layout';
import { Collapse, Button, Input } from 'antd';
import MyForm from '@/components/MyForm';
import React, { useState } from 'react';
import Style from '@/pages/assets/overview/Detail/index.less';
import { Table, Tag, Space } from 'antd';
import { history } from 'umi';
import { Tabs } from 'antd';
import api from '@/services';
import { useEffect } from 'react';
import dayjs from 'dayjs';
const { TabPane } = Tabs;

const { Panel } = Collapse;
function callback() {}

export default function index() {
  //选项卡
  function callback(key: any) {
    console.log(key);
  }

  // 表单
  const formConfig = {
    config: {
      name: 'formName',
      labelCol: { span: 30 },
      wrapperCol: { span: 30 },
    },
    formItems: [
      {
        id: 10,
        config: {
          label: '',
          name: 'rangePicker',
        },
        item: {
          config: {
            showTime: { format: 'HH:mm' },
            format: 'YYYY-MM-DD HH:mm',
          },
          eleType: 'rangePicker',
        },
      },
    ],
  };

  // 表单
  const [editor, setEditor] = useState('');

  const onEditorChange = (newValue: any) => {
    setEditor(newValue);
  };

  const onFinish = (values: any) => {
    values.editor = editor;
    console.log(values);
  };

  const onFinishFailed = (values: any) => {
    console.log(values);
  };

  //数据表格
  const columns = [
    {
      title: '流水号',
      dataIndex: 'settlement_no',
      key: 'settlement_no',
    },
    {
      title: '订单金额',
      dataIndex: 'order_money',
      key: 'order_money',
    },
    {
      title: '店铺退款金额',
      dataIndex: 'refund_shop_money',
      key: 'refund_shop_money',
    },
    {
      title: '平台抽成',
      dataIndex: 'platform_money',
      key: 'platform_money',
    },
    {
      title: '店铺金额',
      dataIndex: 'shop_money',
      key: 'shop_money',
    },
    {
      title: '结算状态',
      dataIndex: 'is_settlement',
      key: 'is_settlement',
    },
    {
      title: '完成时间',
      dataIndex: 'end_time',
      key: 'end_time',
    },
  ];

  const columns2 = [
    {
      title: '流水号',
      dataIndex: 'settlement_no',
      key: 'settlement_no',
    },
    {
      title: '订单金额',
      dataIndex: 'order_money',
      key: 'order_money',
    },
    {
      title: '店铺退款金额',
      dataIndex: 'refund_shop_money',
      key: 'refund_shop_money',
    },
    {
      title: '平台抽成',
      dataIndex: 'platform_money',
      key: 'platform_money',
    },
    {
      title: '店铺金额',
      dataIndex: 'shop_money',
      key: 'shop_money',
    },
    {
      title: '结算状态',
      dataIndex: 'is_settlement',
      key: 'is_settlement',
    },
    {
      title: '完成时间',
      dataIndex: 'end_time',
      key: 'end_time',
    },
  ];

  const columns3 = [
    {
      title: '流水号',
      dataIndex: 'settlement_no',
      key: 'settlement_no',
    },
    {
      title: '订单金额',
      dataIndex: 'order_money',
      key: 'order_money',
    },
    {
      title: '店铺退款金额',
      dataIndex: 'refund_shop_money',
      key: 'refund_shop_money',
    },
    {
      title: '平台抽成',
      dataIndex: 'platform_money',
      key: 'platform_money',
    },
    {
      title: '店铺金额',
      dataIndex: 'shop_money',
      key: 'shop_money',
    },
    {
      title: '结算状态',
      dataIndex: 'is_settlement',
      key: 'is_settlement',
    },
    {
      title: '完成时间',
      dataIndex: 'end_time',
      key: 'end_time',
    },
  ];

  //查看明细
  const [data, setdata] = useState([]);
  const [data2, setdata2] = useState([]);
  const [data3, setdata3] = useState([]);
  const [noSettle, setSettle] = useState(0);
  const getDetail = async () => {
    const res: any = await api.reqLookDetail();
    console.log(res);
    const list = res.data.data.map((item: any) => {
      return {
        key: item.id.toString(),
        settlement_no: item.settlement_no,
        order_money: item.order_money.toFixed(2),
        refund_shop_money: item.refund_shop_money.toFixed(2),
        platform_money: item.platform_money.toFixed(2),
        shop_money: item.shop_money.toFixed(2),
        is_settlement: item.is_settlement ? '已结算' : '未结算',
        end_time: dayjs(item.end_time).format('YYYY-MM-DD HH:MM:ss'),
      };
    });
    //data
    const list1 = list.filter((item: any) => item.is_settlement === '未结算');
    setdata(list1);
    console.log(list1);

    //待结算金额或未结算
    const price = list1.reduce((pre: any, item: any) => {
      return pre + Number(item.order_money);
    }, 0);
    setSettle(price.toFixed(2));

    //data3
    const list3 = list.filter((item: any) => item.is_settlement === '已结算');
    setdata3(list3);
  };

  //data2
  const getDetail2 = async () => {
    const res: any = await api.reqLookDetail();
    console.log(res);
    const list2 = res.data.data.map((item: any) => {
      return {
        key: item.id.toString(),
        settlement_no: item.settlement_no,
        order_money: item.order_money.toFixed(2),
        refund_shop_money: item.refund_shop_money.toFixed(2),
        platform_money: item.platform_money.toFixed(2),
        shop_money: item.shop_money.toFixed(2),
        is_settlement: item.is_settlement ? '已结算' : '结算中',
        end_time: dayjs(item.end_time).format('YYYY-MM-DD HH:MM:ss'),
      };
    });
    const list2_1 = list2.filter(
      (item: any) => item.is_settlement === '结算中',
    );
    setdata2(list2_1);
  };

  // 生命周期
  useEffect(() => {
    getDetail();
    getDetail2();
  }, []);

  return (
    <div>
      {/* 提示 */}
      <PageContainer
        style={{ height: '50px', overflow: 'hidden' }}
      ></PageContainer>

      {/* 店铺结算 */}
      <div
        style={{
          borderLeft: '3px solid #1890FF',
          fontWeight: 'bold',
          marginLeft: '20px',
          paddingLeft: '10px',
          marginTop: '20px',
        }}
      >
        交易金额
      </div>
      <div
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          paddingLeft: '50px',
          paddingRight: '50px',
          paddingTop: '35px',
          color: '#000000',
          marginBottom: '30px',
        }}
      >
        <div className={Style.over_content1}>
          <div>待结算（元）</div>
          <div className={Style.storede_profit}>{noSettle}</div>
        </div>
        <div className={Style.over_content1}>
          <div>进行中（元）</div>
          <div className={Style.storede_profit}>{noSettle}</div>
        </div>
        <div className={Style.over_content1}>
          <div>已结算（元）</div>
          <div className={Style.storede_profit}>0.00</div>
        </div>
      </div>

      <div
        style={{
          borderLeft: '3px solid #1890FF',
          fontWeight: 'bold',
          marginLeft: '20px',
          paddingLeft: '10px',
          marginTop: '0px',
        }}
      >
        待结算订单
      </div>

      {/* 时间表单 */}
      <div
        style={{
          display: 'flex',
          marginLeft: '900px',
          marginTop: '20px',
          marginBottom: '10px',
        }}
      >
        <div>
          <MyForm
            formConfig={formConfig}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            onEditorChange={onEditorChange}
          />
        </div>
        <div className={Style.over_search}>搜索</div>
      </div>

      {/* 数据表格 */}
      <div>
        <Tabs onChange={callback} type="card">
          <TabPane tab="待结算" key="1">
            <Table columns={columns} dataSource={data} />
          </TabPane>
          <TabPane tab="结算中" key="2">
            <Table columns={columns2} dataSource={data2} />
          </TabPane>
          <TabPane tab="已结算" key="3">
            <Table columns={columns3} dataSource={data3} />
          </TabPane>
        </Tabs>
      </div>
    </div>
  );
}
