import { PageContainer } from '@ant-design/pro-layout';
import { Collapse, Button, Input } from 'antd';
import MyForm from '@/components/MyForm';
import React, { useState, useEffect } from 'react';
import Style from '@/pages/assets/overview/Overview/index.less';
import { Table, Tag, Space } from 'antd';
import { history } from 'umi';
import { Tabs } from 'antd';
import api from '@/services';
import dayjs from 'dayjs';
import { DatePicker } from 'antd';
import moment from 'moment';
const { TabPane } = Tabs;

const { Panel } = Collapse;
function callback() {}

export default function index() {
  //选项卡
  function callback(key: any) {
    console.log(key);
  }

  // 日期选择器
  const { RangePicker } = DatePicker;
  const [startTime, setStartTime] = useState(0);
  const [endTime, setEndTime] = useState(0);

  function onChange(dates: any, dateStrings: any) {
    console.log(dates, dateStrings);

    const [startTime, endTime] = dateStrings;
    const start = new Date(startTime).getTime();
    const end = new Date(endTime).getTime();
    setStartTime(start); //开始时间
    setEndTime(end); //结束时间
  }

  //筛选时间
  const selectTime = async () => {
    //  console.log(startTime,endTime)
    let res: any = await api.reqOverTime({ startTime, endTime });
    console.log(res);
    const timeList = res.data.data.map((item: any) => {
      return {
        key: item.order_id.toString(),
        order_no: item.order_no,
        order_from: item.order_from,
        price: item.price.toFixed(2),
        order_from_name: item.order_from_name,
        pay_time: dayjs(item.pay_time).format('YYYY-MM-DD HH:MM:ss'),
        recharge_name: item.recharge_name,
      };
    });
    // 全部
    setdata(timeList);

    // 收入订单列表
    const timeList2 = timeList.filter((item: any) => Number(item.price) !== 0);
    setdata2(timeList2);

    // 支出订单列表
    const timeList3 = timeList.filter(
      (item: any) => item.order_from_name === '支出',
    );
    setdata3(timeList3);

    //待结算
    const timeList2_1 = res.data.data.filter(
      (item: any) => Number(item.price) === 0,
    );
    const price2 = timeList2_1.reduce((pre: any, item: any) => {
      return pre + Number(item.buy_price);
    }, 0);
    setSettle(price2.toFixed(2));

    //店铺总收入
    const price = timeList.reduce((pre: any, item: any) => {
      return pre + Number(item.price);
    }, 0);
    setincome(price.toFixed(2));
  };

  //数据表格
  const columns = [
    {
      title: '账单编号',
      dataIndex: 'order_no',
      key: 'order_no',
    },
    {
      title: '收支来源',
      dataIndex: 'order_from',
      key: 'order_from',
    },
    {
      title: '金额',
      dataIndex: 'price',
      key: 'price',
    },
    {
      title: '收支类型',
      dataIndex: 'order_from_name',
      key: 'order_from_name',
    },
    {
      title: '时间',
      dataIndex: 'pay_time',
      key: 'pay_time',
    },
    {
      title: '说明',
      dataIndex: 'recharge_name',
      key: 'recharge_name',
    },
  ];

  const columns2 = [
    {
      title: '账单编号',
      dataIndex: 'order_no',
      key: 'order_no',
    },
    {
      title: '收支来源',
      dataIndex: 'order_from',
      key: 'order_from',
    },
    {
      title: '金额',
      dataIndex: 'price',
      key: 'price',
    },
    {
      title: '收支类型',
      dataIndex: 'order_from_name',
      key: 'order_from_name',
    },
    {
      title: '时间',
      dataIndex: 'pay_time',
      key: 'pay_time',
    },
    {
      title: '说明',
      dataIndex: 'recharge_name',
      key: 'recharge_name',
    },
  ];

  const columns3 = [
    {
      title: '账单编号',
      dataIndex: 'order_no',
      key: 'order_no',
    },
    {
      title: '收支来源',
      dataIndex: 'order_from',
      key: 'order_from',
    },
    {
      title: '金额',
      dataIndex: 'price',
      key: 'price',
    },
    {
      title: '收支类型',
      dataIndex: 'order_from_name',
      key: 'order_from_name',
    },
    {
      title: '时间',
      dataIndex: 'pay_time',
      key: 'pay_time',
    },
    {
      title: '说明',
      dataIndex: 'recharge_name',
      key: 'recharge_name',
    },
  ];

  //点击跳转查看明细页面
  const toDetail = () => {
    history.push('/assets/overview/Detail');
  };

  //获取资产概况
  const [data, setdata] = useState([]);
  const [data2, setdata2] = useState([]);
  const [data3, setdata3] = useState([]);
  const [income, setincome] = useState(0);
  const [noSettle, setSettle] = useState(0);
  const getView = async () => {
    const res: any = await api.reqOverview();
    console.log(res);
    const list = res.data.data.map((item: any) => {
      return {
        key: item.order_id.toString(),
        order_no: item.order_no,
        order_from: item.order_from,
        price: item.price.toFixed(2),
        order_from_name: item.order_from_name,
        pay_time: dayjs(item.pay_time).format('YYYY-MM-DD HH:MM:ss'),
        recharge_name: item.recharge_name,
      };
    });
    // data全部订单列表
    setdata(list);
    console.log(list);

    // data2收入订单列表
    const list2 = list.filter((item: any) => Number(item.price) !== 0);
    setdata2(list2);
    console.log(list2);

    //data3支出订单列表
    const list3 = list.filter((item: any) => item.order_from_name === '支出');
    setdata3(list3);

    //待结算
    const list2_1 = res.data.data.filter(
      (item: any) => Number(item.price) === 0,
    );
    const price2 = list2_1.reduce((pre: any, item: any) => {
      return pre + Number(item.buy_price);
    }, 0);
    setSettle(price2.toFixed(2));

    //店铺总收入
    const price = list.reduce((pre: any, item: any) => {
      return pre + Number(item.price);
    }, 0);
    setincome(price.toFixed(2));
  };

  //生命周期函数
  useEffect(() => {
    getView();
  }, []);

  return (
    <div>
      {/* 提示 */}
      <PageContainer
        style={{ height: '50px', overflow: 'hidden' }}
      ></PageContainer>

      {/* 店铺结算 */}
      <div
        style={{
          borderLeft: '3px solid #1890FF',
          fontWeight: 'bold',
          marginLeft: '20px',
          paddingLeft: '10px',
          marginTop: '20px',
        }}
      >
        账户概况
      </div>
      <div
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          paddingLeft: '50px',
          paddingRight: '50px',
          paddingTop: '35px',
          color: '#000000',
          marginBottom: '30px',
        }}
      >
        <div className={Style.over_content1}>
          <div>店铺总收入（元）</div>
          <div className={Style.storede_profit}>{income}</div>
        </div>
        <div className={Style.over_content1}>
          <div>可用余额（元）</div>
          <div className={Style.storede_profit}>990.00</div>
        </div>
        <div className={Style.over_content1}>
          <div>
            待结算（元）
            <span
              style={{ color: '#1890FF', cursor: 'pointer' }}
              onClick={toDetail}
            >
              查看明细
            </span>
          </div>
          <div className={Style.storede_profit}>{noSettle}</div>
        </div>
      </div>

      <div
        style={{
          borderLeft: '3px solid #1890FF',
          fontWeight: 'bold',
          marginLeft: '20px',
          paddingLeft: '10px',
          marginTop: '0px',
        }}
      >
        收支记录
      </div>

      {/* 时间表单 */}
      <div
        style={{
          display: 'flex',
          marginLeft: '900px',
          marginTop: '20px',
          marginBottom: '10px',
        }}
      >
        <div>
          <Space direction="vertical" size={12}>
            <RangePicker
              ranges={{
                Today: [moment(), moment()],
                'This Month': [
                  moment().startOf('month'),
                  moment().endOf('month'),
                ],
              }}
              showTime
              format="YYYY/MM/DD HH:mm:ss"
              onChange={onChange}
            />
          </Space>
        </div>
        <div className={Style.over_search} onClick={selectTime}>
          搜索
        </div>
      </div>

      {/* 数据表格 */}
      <div>
        <Tabs onChange={callback} type="card">
          <TabPane tab="全部" key="1">
            <Table columns={columns} dataSource={data} />
          </TabPane>
          <TabPane tab="收入" key="2">
            <Table columns={columns2} dataSource={data2} />
          </TabPane>
          <TabPane tab="支出" key="3">
            <Table columns={columns3} dataSource={data3} />
          </TabPane>
        </Tabs>
      </div>
    </div>
  );
}
