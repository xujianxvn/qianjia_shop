import { PageContainer } from '@ant-design/pro-layout';
import { Collapse, Button, Input } from 'antd';
import MyForm from '@/components/MyForm';
import React, { useState, useEffect } from 'react';
import Style from '@/pages/assets/shopSettlement/ShopSettlement/index.less';
import { Table, Tag, Space } from 'antd';
import { history } from 'umi';
import { Modal } from 'antd';
import api from '@/services';
import dayjs from 'dayjs';

const { Panel } = Collapse;
function callback() {}

export default function index() {
  // 表单
  const formConfig = {
    config: {
      name: 'formName',
      labelCol: { span: 30 },
      wrapperCol: { span: 30 },
    },
    formItems: [
      {
        id: 10,
        config: {
          label: '',
          name: 'rangePicker',
        },
        item: {
          config: {
            showTime: { format: 'HH:mm' },
            format: 'YYYY-MM-DD HH:mm',
          },
          eleType: 'rangePicker',
        },
      },
    ],
  };

  // 表单
  const [editor, setEditor] = useState('');

  const onEditorChange = (newValue: any) => {
    setEditor(newValue);
  };

  const onFinish = (values: any) => {
    values.editor = editor;
    console.log(values);
  };

  const onFinishFailed = (values: any) => {
    console.log(values);
  };

  // 表单2
  const formConfig2 = {
    config: {
      name: 'formName',
      labelCol: { span: 30 },
      wrapperCol: { span: 30 },
    },
    formItems: [
      {
        id: 4,
        config: {
          label: '结算备注',
          name: 'textarea',
        },
        item: {
          config: {
            placeholder: '请输入内容',
          },
          eleType: 'textarea',
        },
      },
    ],
  };

  // 表单2

  const onEditorChange2 = (newValue: any) => {
    setEditor(newValue);
  };

  const onFinish2 = (values: any) => {
    values.editor = editor;
    console.log(values);
  };

  const onFinishFailed2 = (values: any) => {
    console.log(values);
  };

  //数据表格
  const columns = [
    {
      title: '流水号',
      dataIndex: 'settlement_no',
      key: 'settlement_no',
    },
    {
      title: '门店',
      dataIndex: 'store_name',
      key: 'store_name',
    },
    {
      title: '订单总额',
      dataIndex: 'shop_money',
      key: 'shop_money',
    },
    {
      title: '线上结算金额',
      dataIndex: 'order_money',
      key: 'order_money',
    },
    {
      title: '线下结算金额',
      dataIndex: 'offline_order_mone',
      key: 'offline_order_mone',
    },
    {
      title: '结算开始时间',
      dataIndex: 'start_time',
      key: 'start_time',
    },
    {
      title: '结算结束时间',
      dataIndex: 'end_time',
      key: 'end_time',
    },
    {
      title: '是否结算',
      dataIndex: 'is_settlement',
      key: 'is_settlement',
    },
    {
      title: '操作',
      key: 'action',
      render: (text: any) => (
        <Space size="middle">
          <a onClick={toSettle}>结算</a>
          <a onClick={toDetail} style={{ cursor: 'pointer' }}>
            详情
          </a>
        </Space>
      ),
    },
  ];

  //点击详情，跳转详情页
  const toDetail = () => {
    history.push('/assets/shopSettlement/ShopDetail');
  };

  //点击结算显示弹出框
  const toSettle = () => {
    setIsModalVisible(true);
  };

  //点击结算弹出款
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  //获取门店结算列表
  const [data, setdata] = useState([]);
  const getList = async () => {
    const res: any = await api.reqLookDetail();
    const list = res.data.data.map((item: any) => {
      return {
        key: item.id.toString(),
        settlement_no: item.settlement_no,
        store_name: item.store_name,
        shop_money: item.shop_money.toFixed(2),
        order_money: item.order_money.toFixed(2),
        offline_order_mone: item.offline_refund_money.toFixed(2),
        start_time: dayjs(item.start_time).format('YYYY-MM-DD HH:MM:ss'),
        end_time: dayjs(item.end_time).format('YYYY-MM-DD HH:MM:ss'),
        is_settlement: item.is_settlement ? '是' : '否',
      };
    });
    setdata(list);
  };

  //生命周期
  useEffect(() => {
    getList();
  }, []);

  return (
    <div>
      {/* 提示 */}
      <PageContainer
        style={{ height: '50px', overflow: 'hidden' }}
      ></PageContainer>
      <Collapse
        defaultActiveKey={['1']}
        onChange={callback}
        style={{ marginTop: 20 }}
      >
        <Panel header="操作提示" key="1">
          <ul style={{ color: 'gray' }}>
            <li style={{ lineHeight: 3 }}>
              页面展示商家所有的门店进行的结算操作。
            </li>
            <li style={{ lineHeight: 3 }}>
              商家可在此查看结算详情以及结算操作。
            </li>
          </ul>
        </Panel>
      </Collapse>

      {/* 时间表单 */}
      <div
        style={{
          display: 'flex',
          marginLeft: '900px',
          marginTop: '20px',
          marginBottom: '20px',
        }}
      >
        <div>
          <MyForm
            formConfig={formConfig}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            onEditorChange={onEditorChange}
          />
        </div>
        <div className={Style.shop_search}>搜索</div>
      </div>

      {/* 数据表格 */}
      <div>
        <Table columns={columns} dataSource={data} />
      </div>

      {/* 点击结算弹出框 */}
      <div>
        <Modal
          title="结算"
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <MyForm
            formConfig={formConfig2}
            onFinish={onFinish2}
            onFinishFailed={onFinishFailed2}
            onEditorChange={onEditorChange2}
          />
        </Modal>
      </div>
    </div>
  );
}
