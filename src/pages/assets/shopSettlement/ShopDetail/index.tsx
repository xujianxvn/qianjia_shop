import { PageContainer } from '@ant-design/pro-layout';
import { Collapse, Button, Input } from 'antd';
import MyForm from '@/components/MyForm';
import React, { useState } from 'react';
import Style from '@/pages/assets/shopSettlement/ShopDetail/index.less';
import { Table, Tag, Space } from 'antd';
import { history } from 'umi';
import api from '@/services';
import { useEffect } from 'react';
import dayjs from 'dayjs';

const { Panel } = Collapse;
function callback() {}

export default function index() {
  //数据表格
  const columns = [
    {
      title: '订单编号',
      dataIndex: 'order_no',
      key: 'order_no',
    },
    {
      title: '支付方式',
      dataIndex: 'pay_type',
      key: 'pay_type',
    },
    {
      title: '订单销售额',
      dataIndex: 'order_money',
      key: 'order_money',
    },
    {
      title: '订单退款',
      dataIndex: 'refund_money',
      key: 'refund_money',
    },
    {
      title: '佣金',
      dataIndex: 'commission',
      key: 'commission',
    },
    {
      title: '平台优惠券',
      dataIndex: 'coupon_money',
      key: 'coupon_money',
    },
    {
      title: '平台优惠券（退款部分）',
      dataIndex: 'promotion_money',
      key: 'promotion_money',
    },
    {
      title: '店铺收入',
      dataIndex: 'shop_money',
      key: 'shop_money',
    },
    {
      title: '订单完成时间',
      dataIndex: 'finish_time',
      key: 'finish_time',
    },
  ];

  //获取订单列表
  const [data, setdata] = useState([]);
  const getOrder = async () => {
    const res: any = await api.reqStoreSettle();
    const list = res.data.data.map((item: any) => {
      return {
        key: item.order_id.toString(),
        order_no: item.order_no,
        pay_type: item.pay_type,
        order_money: item.order_money.toFixed(2),
        refund_money: item.refund_money.toFixed(2),
        commission: item.commission.toFixed(2),
        coupon_money: item.coupon_money.toFixed(2),
        promotion_money: item.promotion_money.toFixed(2),
        shop_money: item.goods_money.toFixed(2),
        finish_time: dayjs(item.finish_time).format('YYYY-MM-DD HH:MM:ss'),
      };
    });
    setdata(list);
  };

  //生命周期
  useEffect(() => {
    getOrder();
  }, []);

  return (
    <div>
      {/* 提示 */}
      <PageContainer
        style={{ height: '50px', overflow: 'hidden' }}
      ></PageContainer>
      <Collapse
        defaultActiveKey={['1']}
        onChange={callback}
        style={{ marginTop: 20 }}
      >
        <Panel header="操作提示" key="1">
          <ul style={{ color: 'gray' }}>
            {/* <li style={{ lineHeight: 3 }}>
              账期时间：2021-09-03 00:00:00 至 2021-09-04 00:00:00
            </li> */}
            <li style={{ lineHeight: 3 }}>
              线上结算金额 = 店铺总金额(
              <span style={{ color: 'red' }}>￥0.00</span>) - 退款金额(
              <span style={{ color: 'red' }}>￥0.00</span>) - 佣金金额(
              <span style={{ color: 'red' }}>￥0.00</span>) + 平台优惠券(
              <span style={{ color: 'red' }}>￥0.00</span>) -
              平台优惠券(退款部分)(<span style={{ color: 'red' }}>￥0.00</span>)
            </li>
            <li style={{ lineHeight: 3 }}>
              线下结算金额 = 线下支付的订单金额(
              <span style={{ color: 'red' }}>￥0.00</span>) - 退款金额(
              <span style={{ color: 'red' }}>￥0.00</span>) + 平台优惠券(
              <span style={{ color: 'red' }}>￥0.00</span>) -
              平台优惠券(退款部分)(<span style={{ color: 'red' }}>￥0.00</span>)
            </li>
            <li style={{ lineHeight: 3 }}>
              平台抽成金额(<span style={{ color: 'red' }}>￥0.00</span>) =
              平台结算总抽成(<span style={{ color: 'red' }}>￥0.00</span>) -
              平台退款抽成(<span style={{ color: 'red' }}>￥0.00</span>)
            </li>
            <li style={{ lineHeight: 3 }}>
              线下付款金额<span style={{ color: 'red' }}>￥0.00</span>
            </li>
            <li style={{ lineHeight: 3 }}>
              线下退款金额<span style={{ color: 'red' }}>￥0.00</span>
            </li>
          </ul>
        </Panel>
      </Collapse>

      {/* 数据表格 */}
      <div style={{ marginTop: '35px' }}>
        <Table columns={columns} dataSource={data} />
      </div>
    </div>
  );
}
