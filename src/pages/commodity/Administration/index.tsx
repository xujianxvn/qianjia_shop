import { PageContainer } from '@ant-design/pro-layout';
import React from 'react';
import { Button, Input } from 'antd';
function callback() {}
// 折叠面板提示
import CommonCollapse from '@/components/CommonCollapse';

export default function index() {
  const { Search } = Input;
  const onSearch = (value: any) => console.log(value);
  const collapseText = [
    '平台端维护系统的平台，同时商家可以添加自己店铺的品牌。',
    '商家添加商品时可以选择对应的品牌，品牌可以是平台的品牌也可以是商家品牌。',
    '商品类型可以关联平台的品牌用在前台搜索商品。',
  ];

  return (
    <div>
      {/* 面包屑 */}
      <PageContainer
        style={{ height: '50px', overflow: 'hidden' }}
      ></PageContainer>
      {/* 折叠面板 */}
      <CommonCollapse text1={collapseText} />
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          padding: '0 20px',
        }}
      >
        <Button
          type="primary"
          style={{ margin: '3vh 0', backgroundColor: '#4685FD', border: '0' }}
        >
          添加品牌
        </Button>
        <Search
          placeholder="请输入品牌名称"
          allowClear
          onSearch={onSearch}
          style={{ width: 200 }}
        />
      </div>
    </div>
  );
}
