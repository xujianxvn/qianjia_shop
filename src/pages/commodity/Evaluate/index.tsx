import { PageContainer } from '@ant-design/pro-layout';
import React from 'react';
import { Input, Space } from 'antd';
import { AudioOutlined } from '@ant-design/icons';
const { Search } = Input;
import { Cascader } from 'antd';
// 折叠面板提示
import CommonCollapse from '@/components/CommonCollapse';

export default function index() {
  const onSearch = (value: any) => console.log(value);
  function onChange(value: any) {
    console.log(value);
  }
  const options = [
    {
      code: '0',
      name: '请选择评分类型',
    },
    {
      code: '1',
      name: '好评',
    },
    {
      code: '2',
      name: '中评',
    },
    {
      code: '3',
      name: '差评',
    },
  ];
  const option = [
    {
      code: '0',
      name: '请选择搜索方式',
    },
    {
      code: '1',
      name: '商品名称',
    },
    {
      code: '2',
      name: '评价人名称',
    },
  ];
  const collapseText = [
    '会员购买商品之后可以针对购买的商品进行评价或者追评',
    '商品评价之后会在前台的商品详情中进行显示',
    '商家可以针对会员的商品评价进行回复',
  ];
  return (
    <div>
      <PageContainer
        style={{ height: '50px', overflow: 'hidden' }}
      ></PageContainer>
      <CommonCollapse text1={collapseText} />
      <div
        style={{
          marginTop: 20,
          background: 'white',
          display: 'flex',
          justifyContent: 'flex-end',
          height: 80,
          alignItems: 'center',
        }}
      >
        <Cascader
          fieldNames={{ label: 'name', value: 'code', children: 'items' }}
          options={options}
          onChange={onChange}
          placeholder="请选择评分类型"
          defaultValue={['请选择评分类型']}
        />
        <Cascader
          fieldNames={{ label: 'name', value: 'code', children: 'items' }}
          options={option}
          onChange={onChange}
          placeholder="请选择搜索方式"
          defaultValue={['请选择搜索方式']}
        />
        <Search
          placeholder="请输入商品名称/评价人名称"
          allowClear
          onSearch={onSearch}
          style={{ width: 250 }}
        />
      </div>
    </div>
  );
}
