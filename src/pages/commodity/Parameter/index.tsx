import { PageContainer } from '@ant-design/pro-layout';
import React from 'react';
// 折叠面板提示
import CommonCollapse from '@/components/CommonCollapse';
import { ProColumns } from '@ant-design/pro-table';
// import { TableListItem } from '../List';
function callback() {}
import { SearchOutlined } from '@ant-design/icons';
import { Input, Space, Button } from 'antd';
import { Tooltip } from 'antd';
import { DownOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import ProTable, { TableDropdown } from '@ant-design/pro-table';

export type TableListItem = {
  key: number;
  name: string;
  containers: number;
  creator: string;
  status: string;
  createdAt: number;
  memo: string;
};

const columns: ProColumns<TableListItem>[] = [
  {
    title: '商品参数名称',
    width: '70%',
    dataIndex: 'name',
    render: (_) => <a>{_}</a>,
  },
  {
    title: '排序',
    dataIndex: 'containers',
    width: '15%',
    align: 'right',
    sorter: (a, b) => a.containers - b.containers,
  },

  {
    title: '操作',
    width: '10%',
    key: 'option',
    valueType: 'option',
    render: () => [
      <a key="link">链路</a>,
      <TableDropdown
        key="actionGroup"
        menus={[
          { key: 'copy', name: '复制' },
          { key: 'delete', name: '删除' },
        ]}
      />,
    ],
  },
];

export default function index() {
  const { Search } = Input;
  const onSearch = (value: any) => console.log(value);
  // 折叠面板数据
  const collapseText = [
    '商品参数用在添加或者编辑商品选择类型然后配置对应的商品参数。',
    '商品配置好对应的参数，前台用户可以根据分类关联的参数在搜索分类商品之后根据商品参数进行进一步的搜索。',
    '商家也可添加自己店铺的类型和属性，平台端的商品参数和属性才能参与前台搜索。',
  ];
  return (
    <div>
      <PageContainer
        style={{ height: '50px', overflow: 'hidden' }}
      ></PageContainer>
      <CommonCollapse text1={collapseText} />
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          padding: '0 20px',
        }}
      >
        <Button
          type="primary"
          style={{ margin: '3vh 0', backgroundColor: '#4685FD', border: '0' }}
        >
          添加商品参数
        </Button>
        <Search
          placeholder="请输入商品参数"
          allowClear
          onSearch={onSearch}
          style={{ width: 200 }}
        />
      </div>
      <ProTable<TableListItem> columns={columns} />
    </div>
  );
}
