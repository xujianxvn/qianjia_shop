import { PageContainer } from '@ant-design/pro-layout';
import React from 'react';
// 折叠面板提示
import CommonCollapse from '@/components/CommonCollapse';

export default function index() {
  const collapseText = [
    '被删除的商品会在回收站进行展示',
    '回收站列表中的商品点击删除将会彻底删除，不可恢复',
    '点击恢复可将商品重新展示在商品列表中',
  ];
  return (
    <div>
      <PageContainer
        style={{ height: '50px', overflow: 'hidden' }}
      ></PageContainer>
      <CommonCollapse text1={collapseText} />
    </div>
  );
}
