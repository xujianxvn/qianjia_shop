import React, { useEffect, useState } from 'react';
import { Table } from 'antd';
// import { Goodscategory } from '@/http';
import { Button } from 'antd';
import { history } from 'umi';
import { PageContainer } from '@ant-design/pro-layout';
// function callback() {}
// 折叠面板
import CommonCollapse from '@/components/CommonCollapse';
const columns = [
  {
    title: '分类名称',
    dataIndex: 'category_name',
    key: 'category_name',
    width: '45%',
    render: (_: any) => <div style={{ color: '#666666' }}>{_}</div>,
  },
  {
    title: '图片',
    dataIndex: 'image',
    key: 'image',
    width: '20%',
    expandable: true,
    render: (image: any) => {
      if (image) {
        return <img src={''} style={{ width: '30px' }} />;
      } else {
        return null;
      }
    },
  },
  {
    title: '是否显示',
    dataIndex: '显示',
    width: '20%',
    key: 'address',
    render: () => {
      return <div>显示</div>;
    },
  },
  {
    title: '操作',
    dataIndex: '编辑  删除',
    width: '30%',
    key: 'addre',
    render: () => {
      return (
        <div style={{ color: '#FF6A00', cursor: 'pointer' }}>编辑 删除</div>
      );
    },
  },
];
const rowSelection = {
  onChange: (selectedRowKeys: any, selectedRows: any) => {
    console.log(
      `selectedRowKeys: ${selectedRowKeys}`,
      'selectedRows: ',
      selectedRows,
    );
  },
  onSelect: (record: any, selected: any, selectedRows: any) => {
    console.log(record, selected, selectedRows);
  },
  onSelectAll: (selected: any, selectedRows: any, changeRows: any) => {
    console.log(selected, selectedRows, changeRows);
  },
};
export default function categary() {
  // useEffect(() => {
  //
  // }, [])
  const collapseText = [
    '商城添加商品的时候需要选择对应的商品分类,用户可以根据商品分类搜索商品。',
    '点击商品分类名前“+”符号，显示当前商品分类的下级分类。',
    '商品属性是前台搜索分类查询商品之后可以通过商品的属性进行进一步搜索。',
    '商品分类如果不显示会导致小程序、h5没有该分类以及该分类下的所有商品',
    '可通过拖拽进行分类排序',
  ];
  return (
    <div>
      <PageContainer
        style={{ height: '50px', overflow: 'hidden' }}
      ></PageContainer>
      {/* <div style={{ fontSize: '14px', color: "gray" }}>商品分类</div> */}
      <CommonCollapse text1={collapseText} />
      <Button
        type="primary"
        style={{ margin: '3vh 0', backgroundColor: '#FF6A00', border: '0' }}
      >
        添加商品分类
      </Button>
      <Table
        columns={columns}
        // rowSelection={{ ...rowSelection, checkStrictly }}
        // dataSource={list}
        // rowKey={(record) => record.category_id}
      />
    </div>
  );
}
