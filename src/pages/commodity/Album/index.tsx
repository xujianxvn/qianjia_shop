import { PageContainer } from '@ant-design/pro-layout';
import React from 'react';
import { Table, Switch, Button } from 'antd';
// 表格组件
import CommonTable from '@/components/CommonTable';

const tableData = {
  // 是否有多选框
  checkBox: true,
  // 表头
  columns: [
    { title: '商品信息', dataIndex: 'goodsInfo', key: 'goodsInfo' },
    { title: '价格（元）', dataIndex: 'price', key: 'price' },
    { title: '库存', dataIndex: 'stock', key: 'stock' },
    { title: '销量', dataIndex: 'saleNum', key: 'saleNum' },
    {
      title: '商品状态',
      dataIndex: 'goodsState',
      key: 'goodsState',
      render: (a: any, b: any) => {
        return (
          <Switch
            checkedChildren="销售中"
            unCheckedChildren="仓库中"
            defaultChecked={b.goodsState === '1' ? true : false}
          />
        );
      },
    },
    { title: '创建时间', dataIndex: 'createTime', key: 'createTime' },
    {
      title: '操作',
      dataIndex: 'operation',
      key: 'operation',
      render: (item: any, row: any) => {
        return (
          <div className="operation">
            <Button type="primary">通过</Button>
            <Button danger>拒绝</Button>
          </div>
        );
      },
    },
  ],
  // 表格数据
  dataSource: [
    {
      id: '1',
      goodsInfo: '可乐',
      price: '3.00',
      stock: '555',
      saleNum: '25',
      goodsState: '1',
      createTime: '2021-09-02',
    },
    {
      id: '2',
      goodsInfo: '瓜子',
      price: '9.00',
      stock: '444',
      saleNum: '15',
      goodsState: '0',
      createTime: '2021-09-02',
    },
    {
      id: '3',
      goodsInfo: '雪碧',
      price: '2.99',
      stock: '333',
      saleNum: '18',
      goodsState: '0',
      createTime: '2021-09-02',
    },
  ],
};
export default function index() {
  // 多选框事件
  const rowSelection = {
    onChange(selectedRowKeys: any) {
      console.log(selectedRowKeys);
    },
  };

  return (
    <div>
      <PageContainer
        style={{ height: '50px', overflow: 'hidden' }}
      ></PageContainer>
      <div
        style={{ background: 'pink', padding: '5px', marginBottom: '15px' }}
      ></div>
      <CommonTable tableData={tableData} />

      <Table
        columns={tableData.columns}
        dataSource={tableData.dataSource}
        rowKey="id"
        bordered
        rowSelection={rowSelection}
      />
    </div>
  );
}
