import React, { useEffect, useState } from 'react';
import ProCard, { StatisticCard } from '@ant-design/pro-card';
import RcResizeObserver from 'rc-resize-observer';
import { Card } from 'antd';
import { history } from '@/.umi/core/history';
const { Statistic } = StatisticCard;
import { Row, Col, Divider } from 'antd';
const style = { background: '#0092ff', padding: '8px 0' };

export default () => {
  const [responsive, setResponsive] = useState(false);
  const [state, setstate] = useState(2);
  const torenewal = () => {
    history.push('shop/administration/settled/Renewal');
  };

  return (
    <div>
      <div
        style={{
          padding: '30px 0 0 30px',
          marginTop: 20,
          background: 'white',
          boxSizing: 'border-box',
        }}
      >
        {state < 15 && (
          <div
            style={{ width: '100%', height: '75px', backgroundColor: 'white' }}
          >
            <div style={{ display: 'flex ' }}>
              <img
                src="https://multi.niuteam.cn/app/shop/view/public/img/warning.png"
                style={{ width: '39px', height: '40px' }}
              />
              <div style={{ paddingLeft: '30px' }}>
                <div>
                  {state < 1 && (
                    <span
                      style={{
                        fontSize: '15px',
                        color: '#000000',
                        fontWeight: 700,
                      }}
                    >
                      店铺已暂停服务，无法正常营业
                    </span>
                  )}
                  {state > 1 && (
                    <span
                      style={{
                        fontSize: '15px',
                        color: '#000000',
                        fontWeight: 700,
                      }}
                    >
                      请尽快续签，否则店铺无法正常运行
                    </span>
                  )}
                  <span
                    style={{
                      color: '#ff6a00',
                      fontSize: '12px',
                      padding: '20px',
                    }}
                    onClick={torenewal}
                  >
                    立即认证
                  </span>
                </div>
                <div style={{ color: '#ff0000', fontSize: '14px ' }}>
                  剩余{state}天
                  {state < 1 && (
                    <span style={{ color: '#5d5c5c', paddingLeft: '10px' }}>
                      (已到期 )
                    </span>
                  )}
                  {state < 14 && state > 1 && (
                    <span style={{ color: '#5d5c5c', paddingLeft: '10px' }}>
                      (尽快续签)
                    </span>
                  )}
                  <span
                    style={{
                      color: '#5d5c5c',
                      fontSize: '14px',
                      paddingLeft: '30px',
                    }}
                  >
                    咨询电话：400-000-000
                  </span>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>

      {/* 概况 */}
      <div style={{ width: '100%' }}>
        <div style={{ display: 'flex' }}>
          <div
            style={{
              fontWeight: 500,
              fontSize: 14,
              lineHeight: 5,
              color: '#333',
              marginLeft: '50px',
            }}
          >
            实时概况
          </div>
          <div
            style={{
              marginLeft: 20,
              color: '#B7B8B7',
              fontSize: 12,
              lineHeight: 6,
            }}
          >
            更新時間:2020-10-13
          </div>
        </div>
      </div>
      {/* top个人推荐 */}

      {state > 14 && (
        <div
          style={{
            width: '99.3%',
            height: '150px',
            background: 'white',
            padding: 20,
            margin: '10px 10px',
          }}
        >
          <div style={{ display: 'flex' }}>
            <div
              style={{
                height: 60,
                width: 60,
                borderRadius: '50%',
                background: 'red',
                margin: '0 20px',
              }}
            ></div>
            <div>
              <div style={{ fontSize: 20, color: '#3c4a54', fontWeight: 1200 }}>
                欢迎体验
              </div>
              <div style={{ color: '#999', marginTop: 10 }}>
                高性能 / 精致 / 优雅。基于Vue3 + Element-Plus
                的中后台前端解决方案，如果喜欢就点个星星支持一下。
              </div>
              <img
                src="https://gitee.com/lolicode/scui/badge/star.svg?theme=dark"
                style={{ marginTop: 15 }}
              />
            </div>
          </div>
        </div>
      )}
      {/* 4个卡片 */}

      <Row gutter={16} style={{ marginBottom: '20px' }}>
        <Col className="gutter-row" span={6}>
          <Card title="今日订单数" extra={<a href="#">昨日</a>}>
            <p style={{ color: 'orange', fontSize: '48px', margin: '0' }}>0</p>
            <br></br>
            <div>
              <p style={{ float: 'left', width: '50%' }}>日同比0%</p>
              <p style={{ float: 'right' }}>昨日订单数0</p>
            </div>
            <br></br>
            <div>
              <p style={{ float: 'left', width: '50%' }}>订单总数</p>
              <p style={{ float: 'right' }}>733</p>
            </div>
          </Card>
        </Col>
        <Col className="gutter-row" span={6}>
          <Card title="今日销售额" extra={<a href="#">昨日</a>}>
            <p style={{ color: 'orange', fontSize: '48px', margin: '0' }}>
              0.00
            </p>
            <br></br>
            <div>
              <p style={{ float: 'left', width: '50%' }}>日同比0%</p>
              <p style={{ float: 'right' }}>昨日销售额（元）0.00</p>
            </div>
            <br></br>
            <div>
              <p style={{ float: 'left', width: '50%' }}>订单总数</p>
              <p style={{ float: 'right' }}>733</p>
            </div>
          </Card>
        </Col>
        <Col className="gutter-row" span={6}>
          <Card title="今日新增会员数" extra={<a href="#">昨日</a>}>
            <p style={{ color: 'orange', fontSize: '48px', margin: '0' }}>
              0.00
            </p>
            <br></br>
            <div>
              <p style={{ float: 'left', width: '50%' }}>日同比-100.00%</p>
              <p style={{ float: 'right' }}>昨日新增会员数0</p>
            </div>
            <br></br>
            <div>
              <p style={{ float: 'left', width: '50%' }}>会员总数</p>
              <p style={{ float: 'right' }}>156</p>
            </div>
          </Card>
        </Col>
        <Col className="gutter-row" span={6}>
          <Card title="今日浏览量" extra={<a href="#">昨日</a>}>
            <p style={{ color: 'orange', fontSize: '48px', margin: '0' }}>0</p>
            <br></br>
            <div>
              <p style={{ float: 'left', width: '50%' }}>日同比0%</p>
              <p style={{ float: 'right' }}>昨日浏览量0</p>
            </div>
            <br></br>
            <div>
              <p style={{ float: 'left', width: '50%' }}>总浏览量</p>
              <p style={{ float: 'right' }}>2032</p>
            </div>
          </Card>
        </Col>
      </Row>

      {/* 总分/业绩目标 */}
      <RcResizeObserver
        key="resize-observer"
        onResize={(offset) => {
          setResponsive(offset.width < 596);
        }}
      >
        <ProCard split={responsive ? 'horizontal' : 'vertical'}>
          <StatisticCard
            colSpan={responsive ? 24 : 6}
            title="财年业绩目标"
            statistic={{
              value: 82.6,
              suffix: '亿',
              description: (
                <Statistic title="日同比" value="6.47%" trend="up" />
              ),
            }}
            chart={
              <img
                src="https://gw.alipayobjects.com/zos/alicdn/PmKfn4qvD/mubiaowancheng-lan.svg"
                alt="进度条"
                width="100%"
              />
            }
            footer={
              <>
                <Statistic
                  value="70.98%"
                  title="财年业绩完成率"
                  layout="horizontal"
                />
                <Statistic
                  value="86.98%"
                  title="去年同期业绩完成率"
                  layout="horizontal"
                />
                <Statistic
                  value="88.98%"
                  title="前年同期业绩完成率"
                  layout="horizontal"
                />
              </>
            }
          />
          <StatisticCard.Group
            colSpan={responsive ? 24 : 18}
            direction={responsive ? 'column' : undefined}
          >
            <StatisticCard
              statistic={{
                title: '财年总收入',
                value: 601987768,
                description: (
                  <Statistic title="日同比" value="6.15%" trend="up" />
                ),
              }}
              chart={
                <img
                  src="https://gw.alipayobjects.com/zos/alicdn/zevpN7Nv_/xiaozhexiantu.svg"
                  alt="折线图"
                  width="100%"
                />
              }
            >
              <Statistic
                title="大盘总收入"
                value={1982312}
                layout="vertical"
                description={
                  <Statistic title="日同比" value="6.15%" trend="down" />
                }
              />
            </StatisticCard>
            <StatisticCard
              statistic={{
                title: '当日排名',
                value: 6,
                description: (
                  <Statistic title="日同比" value="3.85%" trend="down" />
                ),
              }}
              chart={
                <img
                  src="https://gw.alipayobjects.com/zos/alicdn/zevpN7Nv_/xiaozhexiantu.svg"
                  alt="折线图"
                  width="100%"
                />
              }
            >
              <Statistic
                title="近7日收入"
                value={17458}
                layout="vertical"
                description={
                  <Statistic title="日同比" value="6.47%" trend="up" />
                }
              />
            </StatisticCard>
            <StatisticCard
              statistic={{
                title: '财年业绩收入排名',
                value: 2,
                description: (
                  <Statistic title="日同比" value="6.47%" trend="up" />
                ),
              }}
              chart={
                <img
                  src="https://gw.alipayobjects.com/zos/alicdn/zevpN7Nv_/xiaozhexiantu.svg"
                  alt="折线图"
                  width="100%"
                />
              }
            >
              <Statistic
                title="月付费个数"
                value={601}
                layout="vertical"
                description={
                  <Statistic title="日同比" value="6.47%" trend="down" />
                }
              />
            </StatisticCard>
          </StatisticCard.Group>
        </ProCard>
      </RcResizeObserver>
    </div>
  );
};
