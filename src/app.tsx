import { history } from 'umi';
import {
  BasicLayoutProps,
  Settings as LayoutSettings,
} from '@ant-design/pro-layout';

import HeadDown from '@/components/HeadDown';

export const layout = ({
  initialState,
}: {
  initialState: { settings?: LayoutSettings; currentUser?: null };
}): BasicLayoutProps => {
  return {
    siderWidth: 200,
    rightContentRender: () => <HeadDown />,
    onPageChange: (location: any) => {
      // 全局路由守卫
      if (!sessionStorage.getItem('token')) {
        history.replace('/login');
      } else {
        if (location.pathname === '/login') {
          history.replace('/');
        }
      }
    },
    // logo: () => <div></div>,
    menuHeaderRender: (logo, title) => <div>{title}</div>,
    ...initialState?.settings,
  };
};
