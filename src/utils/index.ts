
//递归树状数据
const recursionTree = (data: any, match: Array<any>, count: number, num=0) => {
  num++
  return data.map((item: any) => {
    const o: any = {}

    match.forEach((propname: Array<any>) => {
      o[propname[1]] = item[propname[0]]
    })

    if (item.children && num <= count) {
      o.children = recursionTree(item.children, match, count, num)	//menuRecursion自己调用自己
    }

    return o
  })
}


/*
	格式化时间
*/

const formatTime = (date: any) => {
	const year = date.getFullYear()
	const month = date.getMonth() + 1
	const day = date.getDate()
	const hour = date.getHours()
	const minute = date.getMinutes()
	const second = date.getSeconds()

	return `${[year, month, day].map(formatNumber).join('-')} ${[hour, minute, second].map(formatNumber).join(':')}`
}

const formatNumber = (n: any) => {
	n = n.toString()
	return n[1] ? n : `0${n}`
}


export default {
  recursionTree,
  formatTime
}