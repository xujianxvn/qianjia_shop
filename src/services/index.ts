import request from '@/services/services';
import { LoginData } from '@/type';

const reqUpload = (data: any) => request('/upload', data, 'POST')

const reqCaptcha = (data: any) => request('/users/captcha', data);

const reqLogin = (data: LoginData) => request('/users/login', data, 'POST');

const reqList = () => request('/order/orderList');

const reqRegister = (data: any) => request('/users/register', data, 'POST');

const reqGoodsAdd = (data: any) => request('/goods/add', data, 'POST');

const reqGoodsList = (data?: any) => request('/goods/list', data)

const reqOverview = () => request('/member/overview'); //资产概况

const reqLookDetail = () => request('/store/settlement/detail'); //查看明细

const reqStoreSettle = () => request('/store/order'); //店铺结算订单

const reqOverTime = (data: any) => request('/member/time', data, 'POST'); //资产概况筛选

const reqSiteInfo = () => request('/site/info');

const reqShopInfo = () => request('/shop/info');

const reqShopEdit = (data: any) => request('/shop/edit', data, 'POST');

const reqGoodsDel = (data: any) => request('/goods/del', data, 'POST');

const reqGoodsCategoryTree = () => request('/goods/category/tree')

const reqGoodsBrandList = () => request('/goods/brand/list')

const getOrderList = (data: any) => request('/order/list', data, 'POST');

const reqmemberCount = () => request('/member/count');

const reqSiteEdit = (data: any) => request('/site/edit', data, 'POST');

const reqAuthInfo = () => request('/auth/info');

const reqAddressInfo = () => request('/adress/info');

const reqmembeQuery = (data: any) => request('/member/query', data, 'POST');

const reorderlist = (data: any) => request('/order/orderlist', data);

const reqOrderList =(data?:any) => request('/order/orderList', data)

export default {
  reqUpload,
  reqCaptcha,
  reqLogin,
  reqList,
  reqRegister,
  reqGoodsAdd,
  reqGoodsList,
  reqGoodsCategoryTree,
  reqGoodsBrandList,
  reqOverview,
  reqLookDetail,
  reqStoreSettle,
  reqOverTime,
  reqSiteInfo,
  reqShopEdit,
  reqGoodsDel,
  getOrderList,
  reqmemberCount,
  reqSiteEdit,
  reqAuthInfo,
  reqShopInfo,
  reqAddressInfo,
  reqmembeQuery,
  reorderlist,
  reqOrderList
};
