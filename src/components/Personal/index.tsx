import { PageContainer } from '@ant-design/pro-layout';
import React from 'react';
import Style from '@/components/Personal/index.less';
import MyForm from '@/components/MyForm';
import { Button, Radio } from 'antd';
import { history } from 'umi';
import { values } from '@umijs/deps/compiled/lodash';

export default function index() {
  // const onChange = (e:any) => {
  //     console.log(e.target.value);

  // };
  const torenewal = () => {
    history.push('Renewal');
  };
  const myclick = () => {
    console.log(onFinish);
  };
  const formConfig = {
    config: {
      name: 'formName',
      labelCol: { span: 6 },
      wrapperCol: { span: 14 },
      scrollToFirstError: true,
    },

    formItems: [
      {
        id: 7,
        config: {
          label: '联系人姓名',
          name: 'name',
          rules: [
            { required: true, message: '联系人姓名不能为空!' },
            {
              pattern: /^[\u4E00-\u9FA5\uf900-\ufa2d·s]{2,20}$/,
              message: '姓名格式不对!',
              validateTrigger: 'blur',
            },
          ],
        },
        item: {
          config: {
            placeholder: '请输入联系人姓名',
          },
          eleType: 'text',
        },
      },

      {
        id: 8,
        config: {
          label: '联系手机',
          name: 'phone',
          rules: [
            { required: true, message: '联系人手机号不能为空!' },
            {
              pattern: /^1[3|4|5|7|8][0-9]{9}$/,
              message: '手机号格式不正确!',
              validateTrigger: 'blur',
            },
          ],
        },
        item: {
          config: {
            placeholder: '请输入联系人手机',
          },
          eleType: 'text',
        },
      },

      {
        id: 10,
        config: {
          label: '身份证号',
          name: 'ID',
          rules: [
            { required: true, message: '联系人身份证不能为空!' },
            {
              pattern:
                /^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/,
              message: '身份证格式不正确!',
              validateTrigger: 'blur',
            },
          ],
        },
        item: {
          config: {
            placeholder: '请输入联系人身份证',
          },
          eleType: 'text',
        },
      },
      {
        id: 11,
        config: {
          label: '身份证正面',
          name: '点击上传',
          valuePropName: 'fileList',
          required: true,
          rules: [{ required: true, message: '身份证不能为空!' }],
        },
        item: {
          config: {
            listType: 'picture-card',
          },
          eleType: 'upload',
        },
      },

      {
        id: 12,
        config: {
          label: '身份证反面',
          name: '点击上传',
          valuePropName: 'fileList',
          rules: [{ required: true, message: '身份证不能为空!' }],
        },
        item: {
          config: {
            listType: 'picture-card',
          },
          eleType: 'upload',
        },
      },

      {
        id: 14,
        config: {},
        item: {
          config: {
            type: 'primary',
            htmlType: 'submit',
          },
          eleType: 'button',
          content: '提交',
        },
      },
    ],
  };

  const onFinish = (values: any) => {
    console.log(values);
  };

  const onFinishFailed = (values: any) => {
    console.log(values);
  };
  return (
    <div>
      <div>
        {/* <Radio.Group onChange={onChange} >
                    <Radio value={1}>公司</Radio>
                    <Radio value={2}>个人</Radio>       
                </Radio.Group> */}
        <div>
          <MyForm
            formConfig={formConfig}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
          />
        </div>
      </div>
      <div className={Style.button}>
        <Button type="primary" onClick={torenewal}>
          上一页
        </Button>
        <Button type="primary" style={{ marginLeft: '50px' }} onClick={myclick}>
          下一页
        </Button>
      </div>
      <div style={{ textAlign: 'center' }}>
        <img
          src="https://multi.niuteam.cn/public/static/img/copyright_logo.png"
          alt=""
          style={{ width: '100px', height: '24px', margin: '50px 0' }}
        />
      </div>
    </div>
  );
}
