import {
  Form,
  AutoComplete,
  Cascader,
  DatePicker,
  Input,
  InputNumber,
  Checkbox,
  Radio,
  Rate,
  Switch,
  Slider,
  Select,
  TreeSelect,
  Upload,
  Modal,
  Button,
} from 'antd';

import { PlusOutlined } from '@ant-design/icons';
import ImgCrop from 'antd-img-crop';
import 'antd/es/modal/style';
import 'antd/es/slider/style';

import { Editor } from '@tinymce/tinymce-react';

interface FormConfig {
  config: object;
  formItems: Array<any>;
}

interface Props {
  formConfig: FormConfig;
  form?: any;
  onFinish: (values: any) => void;
  onFinishFailed: (values: any) => void;
  onEditorChange?: (values: any) => any;
  imagesUpload?: () => void;
  filePicker?: () => void;
}

export default ({
  formConfig,
  form,
  onEditorChange,
  onFinish,
  onFinishFailed,
}: Props) => {
  const uploadButton = (
    <div>
      <PlusOutlined />
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );

  const renderEle = (item: any) => {
    if (item.children) {
    } else {
      switch (item.eleType) {
        case 'editor':
          return (
            <Editor
              onEditorChange={onEditorChange}
              init={{
                language: 'zh_CN',
                height: 500,
                min_height: 400,
                menubar: 'file edit view insert format tools table help',
                plugins: [
                  'advlist link image imagetools lists charmap hr anchor pagebreak searchreplace wordcount visualblocks visualchars code insertdatetime nonbreaking save table directionality help autolink autosave print preview spellchecker fullscreen media emoticons template paste pagebreak',
                ],
                toolbar:
                  'code insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media |  forecolor backcolor emoticons | spellchecker  pagebreak   print preview  fullpage help',
                images_upload_handler: (
                  blobInfo: any,
                  success: any,
                  failure: any,
                ) => {
                  let file = blobInfo.blob();
                  let formData = new FormData();
                  formData.append('upload', file);
                  //调接口
                },
                file_picker_callback: (
                  callback: any,
                  value: any,
                  meta: any,
                ) => {
                  switch (meta.filetype) {
                    case 'image':
                      break;
                    case 'media':
                      break;
                    case 'file':
                      break;
                  }
                },
              }}
            />
          );

        case 'autoComplete':
          return <AutoComplete {...item.config} />;

        case 'cascader':
          return <Cascader {...item.config} />;

        case 'datePicker':
          return <DatePicker {...item.config} />;

        case 'rangePicker':
          return <DatePicker.RangePicker {...item.config} />;

        case 'text':
          return <Input {...item.config} />;

        case 'password':
          return <Input.Password {...item.config} />;

        case 'number':
          return <InputNumber {...item.config} />;

        case 'textarea':
          return <Input.TextArea {...item.config} />;

        case 'checkboxGroup':
          return <Checkbox.Group {...item.config} />;

        case 'radioGroup':
          return <Radio.Group {...item.config} />;

        case 'rate':
          return <Rate {...item.config} />;

        case 'switch':
          return <Switch {...item.config} />;

        case 'slider':
          return <Slider {...item.config} />;

        case 'select':
          return <Select {...item.config} />;

        case 'treeSelect':
          return <TreeSelect {...item.config} />;

        case 'upload':
          return (
            <Upload
              {...item.config}
              action="http://localhost:7001/shop/upload"
              headers={{
                Authorization: `Bearer ${sessionStorage.getItem('token')}`,
              }}
            >
              {uploadButton}
            </Upload>
          );

        case 'imgCropUpload':
          return (
            <ImgCrop rotate>
              <Upload
                {...item.config}
                action="http://localhost:7001/shop/upload"
                headers={{
                  Authorization: `Bearer ${sessionStorage.getItem('token')}`,
                }}
              >
                {uploadButton}
              </Upload>
            </ImgCrop>
          );

        case 'button':
          return <Button {...item.config}>{item.content}</Button>;
      }
    }
  };

  return (
    <Form
      {...formConfig.config}
      form={form}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      {formConfig.formItems.map((fitem: any, index: number) => {
        return (
          <Form.Item {...fitem.config} key={index.toString()}>
            {renderEle(fitem.item)}
          </Form.Item>
        );
      })}
    </Form>
  );
};
