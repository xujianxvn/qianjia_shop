import { PageContainer } from '@ant-design/pro-layout';
import React from 'react';
import Style from '@/components/Company/index.less';
import MyForm from '@/components/MyForm';
import { Button, Radio } from 'antd';
import { history } from 'umi';
import { values } from '@umijs/deps/compiled/lodash';

export default function index() {
  // const onChange = (e:any) => {
  //     console.log(e.target.value);

  // };
  const torenewal = () => {
    history.push('Renewal');
  };
  const myclick = () => {
    console.log(onFinish);
  };
  const formConfig = {
    config: {
      name: 'formName',
      labelCol: { span: 6 },
      wrapperCol: { span: 14 },
      scrollToFirstError: true,
    },

    formItems: [
      // {
      //     id: 1,
      //     config: {
      //         label: '申请类型',
      //         name: "radioGroup",
      //         required: true,
      //     },
      //     item: {
      //         config: {
      //             options: ['公司', '个人']
      //         },
      //         eleType: 'radioGroup',

      //     }

      // },

      {
        id: 2,
        config: {
          label: '公司名称',
          name: 'name',
          rules: [{ required: true, message: '公司名称不能为空!' }],
        },
        item: {
          config: {
            placeholder: '请输入公司名称',
          },
          eleType: 'text',
        },
      },
      {
        id: 3,
        config: {
          label: '公司地址',
          name: 'address',
          rules: [{ required: true, message: '地址不能为空!' }],
        },
        item: {
          config: {
            placeholder: '请输入公司地址',
          },
          eleType: 'text',
        },
      },
      {
        id: 4,
        config: {
          label: '统一社会信用码',
          name: 'number',
          rules: [
            { required: true, message: '统一社会信用码不能为空!' },
            {
              pattern: /[0-9A-HJ-NPQRTUWXY]{2}\d{6}[0-9A-HJ-NPQRTUWXY]{10}/,
              message: '统一社会信用码格式不正确!',
              validateTrigger: 'blur',
            },
          ],
        },
        item: {
          config: {
            placeholder: '请输入统一社会信用码',
          },
          eleType: 'text',
        },
      },

      {
        id: 5,
        config: {
          label: '营业执照电子版',
          name: '点击上传',
          valuePropName: 'fileList',
          rules: [{ required: true, message: '请上传营业执照电子版!' }],
        },
        item: {
          config: {
            listType: 'picture-card',
          },
          eleType: 'upload',
        },
      },

      {
        id: 6,
        config: {
          label: '法定经营范围：',
          name: 'textarea',
        },
        item: {
          config: {
            placeholder: '请输入法定经营范围：',
          },
          eleType: 'textarea',
        },
      },

      {
        id: 7,
        config: {
          label: '法人姓名',
          name: 'fare_name',
          rules: [
            { required: true, message: '法人姓名不能为空!' },
            {
              pattern: /^[\u4E00-\u9FA5\uf900-\ufa2d·s]{2,20}$/,
              message: '姓名格式不对!',
              validateTrigger: 'blur',
            },
          ],
        },
        item: {
          config: {
            placeholder: '请输入法人姓名',
          },
          eleType: 'text',
        },
      },

      {
        id: 8,
        config: {
          label: '法人手机',
          name: 'phone',
          rules: [
            { required: true, message: '法人手机号不能为空!' },
            {
              pattern: /^1[3|4|5|7|8][0-9]{9}$/,
              message: '手机号格式不正确!',
              validateTrigger: 'blur',
            },
          ],
        },
        item: {
          config: {
            placeholder: '请输入法人手机',
          },
          eleType: 'text',
        },
      },

      {
        id: 10,
        config: {
          label: '法人身份证',
          name: 'ID',
          rules: [
            { required: true, message: '法人身份证不能为空!' },
            {
              pattern:
                /^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X)$/,
              message: '身份证格式不正确!',
              validateTrigger: 'blur',
            },
          ],
        },
        item: {
          config: {
            placeholder: '请输入法人身份证',
          },
          eleType: 'text',
        },
      },
      {
        id: 11,
        config: {
          label: '法人身份证正面',
          name: '点击上传',
          valuePropName: 'fileList',
          required: true,
          rules: [{ required: true, message: '法人身份证不能为空!' }],
        },
        item: {
          config: {
            listType: 'picture-card',
          },
          eleType: 'upload',
        },
      },

      {
        id: 12,
        config: {
          label: '法人身份证反面',
          name: '点击上传',
          valuePropName: 'fileList',
          rules: [{ required: true, message: '法人身份证不能为空!' }],
        },
        item: {
          config: {
            listType: 'picture-card',
          },
          eleType: 'upload',
        },
      },

      {
        id: 14,
        config: {},
        item: {
          config: {
            type: 'primary',
            htmlType: 'submit',
          },
          eleType: 'button',
          content: '提交',
        },
      },
    ],
  };

  const onFinish = (values: any) => {
    console.log(values);
  };

  const onFinishFailed = (values: any) => {
    console.log(values);
  };
  return (
    <div>
      <div>
        {/* <Radio.Group onChange={onChange} >
                    <Radio value={1}>公司</Radio>
                    <Radio value={2}>个人</Radio>       
                </Radio.Group> */}
        <div>
          <div>
            <MyForm
              formConfig={formConfig}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
            />
          </div>
        </div>
      </div>
      <div className={Style.button}>
        <Button type="primary" onClick={torenewal}>
          上一页
        </Button>
        <Button type="primary" style={{ marginLeft: '50px' }} onClick={myclick}>
          下一页
        </Button>
      </div>
      <div style={{ textAlign: 'center' }}>
        <img
          src="https://multi.niuteam.cn/public/static/img/copyright_logo.png"
          alt=""
          style={{ width: '100px', height: '24px', margin: '50px 0' }}
        />
      </div>
    </div>
  );
}
