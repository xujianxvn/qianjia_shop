import React from 'react';
import { history } from 'umi';
import { Avatar, Modal, Menu, Dropdown } from 'antd';
import {
  UserOutlined,
  DownOutlined,
  QuestionCircleOutlined,
} from '@ant-design/icons';
import './index.less';

const { confirm } = Modal;

export default () => {
  //下拉菜单点击
  const openMenu = ({ key }: any) => {
    //退出登录
    if (key === '2') {
      confirm({
        title: '提示',
        icon: <QuestionCircleOutlined />,
        content: '确定要退出登录吗?',
        onOk() {
          sessionStorage.clear();
          history.replace('/login');
        },
      });
    }
  };

  const menu = (
    <Menu onClick={openMenu}>
      <Menu.Item key="1">
        <div className="mbtn">修改密码</div>
      </Menu.Item>
      <Menu.Item key="2">
        <div className="mbtn">退出登录</div>
      </Menu.Item>
    </Menu>
  );

  return (
    <div className="wrap">
      <Dropdown
        overlay={menu}
        placement="bottomCenter"
        trigger={['click']}
        arrow
      >
        <div className="head">
          <Avatar icon={<UserOutlined />} />
          <div className="uname">admin</div>
          <DownOutlined />
        </div>
      </Dropdown>
    </div>
  );
};
