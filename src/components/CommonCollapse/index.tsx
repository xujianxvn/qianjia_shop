// 折叠面板提示
import React from 'react';
import { Collapse } from 'antd';

const { Panel } = Collapse;

export default function index(props: any) {
  return (
    <div>
      <Collapse defaultActiveKey={['1']}>
        <Panel header="操作提示" key="1">
          <ul style={{ color: 'gray' }}>
            {props.text1.map((item: String, index: any) => {
              return <li key={index}>{item}</li>;
            })}
          </ul>
        </Panel>
      </Collapse>
      ,
    </div>
  );
}
