import React from 'react';
import { Table, Pagination, Button } from 'antd';

// 多选框选中事件
const onCheckbox = (value: any) => {
  console.log(value);
};

export default function index(props: any) {
  const { columns, dataSource, checkBox } = props.tableData;
  // 多选框事件
  const rowSelection = {
    onChange: onCheckbox,
  };

  return (
    <div>
      <Table
        columns={columns}
        rowSelection={checkBox ? rowSelection : undefined}
        dataSource={dataSource}
        rowKey="id"
        pagination={false}
        bordered
      />
      <div className="display space">
        <div>
          <Button danger>批量删除</Button>
        </div>
        <Pagination
          total={dataSource.length}
          showSizeChanger
          showQuickJumper
          showTotal={(total) => `共 ${total} 条`}
        />
      </div>
    </div>
  );
}
