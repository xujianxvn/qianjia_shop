import { MessageOutlined, DownOutlined } from '@ant-design/icons';
import { Badge, Avatar, Popover, Button, Drawer, Empty } from 'antd';
import Style from '@/components/Test/index.less';
import React, { useState } from 'react';

export default function () {
  // 编辑个人设置内容
  const content = (
    <div className={Style.personal_settings}>
      <p className={Style.p}>个人设置</p>
      <p className={Style.p}>cmd</p>
      <p className={Style.p}>退出登录</p>
    </div>
  );
  // 抽屉
  const [visible, setVisible] = useState(false);
  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };

  return (
    <div className={Style.header_right}>
      {/* 编辑抽屉内容 */}
      <Drawer
        title="新消息"
        placement="right"
        closable={false}
        onClose={onClose}
        visible={visible}
        width={400}
      >
        <div>
          <p>Some contents...</p>
          <p>Some contents...</p>
          <p>Some contents...</p>
        </div>
        <div>
          <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
        </div>
        <div style={{ display: 'flex', position: 'absolute', bottom: '20px' }}>
          <Button type="primary">消息中心</Button>
          <Button style={{ marginLeft: '20px' }}>全部设为已读</Button>
        </div>
      </Drawer>
      {/* 徽标 */}
      <div className={Style.header_news} onClick={showDrawer}>
        <Badge
          count={99}
          size="small"
          style={{ backgroundColor: '#f56c6c6' }}
          title="未读消息"
        >
          <MessageOutlined style={{ fontSize: '20px' }} />
        </Badge>
      </div>
      {/* 个人设置 */}
      <div>
        <Popover
          placement="bottom"
          content={content}
          trigger="click"
          className={Style.header_persona}
        >
          <img
            src="	https://lolicode.gitee.io/scui-doc/demo/img/avatar2.gif"
            className={Style.header_img}
          />
          <div className={Style.header_name}>
            admin
            <DownOutlined style={{ fontSize: '10px', marginLeft: '5px' }} />
          </div>
        </Popover>
      </div>
    </div>
  );
}
