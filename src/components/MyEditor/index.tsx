import { useState } from 'react';
import { Editor } from '@tinymce/tinymce-react';
import api from '@/services'

interface Props {
  onUpload?: (blobInfo: any, success: any, failure: any) => void;
  onPicker?: (callback: any, value: any, meta: any) => void;
  onEditorChange?: (ev: any, editor: any) => void;
}

export default ({ onUpload, onPicker, onEditorChange }: Props) => {
  return (
    <Editor
      onEditorChange={onEditorChange}
      init={{
        language: 'zh_CN',
        height: 500,
        min_height: 400,
        menubar: 'file edit view insert format tools table help',
        plugins: [
          'advlist link image imagetools lists charmap hr anchor pagebreak searchreplace wordcount visualblocks visualchars code insertdatetime nonbreaking save table directionality help autolink autosave print preview spellchecker fullscreen media emoticons template paste pagebreak',
        ],
        toolbar:
          'code insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media |  forecolor backcolor emoticons | spellchecker  pagebreak   print preview  fullpage help',
        images_upload_handler: async (blobInfo: any, success: any, failure: any) => {
          let file = blobInfo.blob();
          let formData = new FormData();
          formData.append('file', file);
          //调接口
          const data: any = await api.reqUpload(formData)
          console.log(data);
          if (data.data.code !== 0) return failure(data.data.message)
          success(data.data.data.path)
        },
        file_picker_callback: (callback: any, value: any, meta: any) => {
          console.log(callback, value, meta);
        },
      }}
    />
  );
};
