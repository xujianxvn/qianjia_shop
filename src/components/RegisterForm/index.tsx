import React, { PureComponent, useState, useEffect } from 'react';
// import $api from '@/http'
import { history } from 'umi';
import { Form, Input, Button, Checkbox, message } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';

export default function login(props: any, state: any) {
  const [url, seturl] = useState('');
  const [codeid, setcodeid] = useState('');
  useEffect(() => {
    myelins();
  }, []);

  const tologin = () => {
    history.replace('Login');
  };

  const myelins = () => {
    //  请求
    // $api.postlist().then(data => {
    //     seturl(data.data.data.img)
    //     setcodeid(data.data.data.id)
    // })
  };

  let [value, setValue] = useState();
  // 成功返回输入值
  const onFinish = (values: any) => {
    console.log('Success:', values);
    // $api.login(values.Username, values.Password, values.Code, codeid).then(data => {
    //   if (data.data.code == 0) {
    //     message.info('登录成功');
    //     localStorage.setItem('token', data.data.data.token)
    //     history.push('@/pages/dianpu/renovation/index')
    //   } else if (data.data.code !== 0) {
    //     message.error(data.data.message);
    //     myclick()
    //   }
    // })
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div style={{ paddingTop: '150px' }}>
      <div
        style={{
          backgroundColor: 'rgba(0,0,0,0.3)',
          width: '500px',
          height: '500px',
          marginLeft: 680,
        }}
      >
        <div
          style={{
            textAlign: 'center',
            fontSize: '30px',
            color: '#666',
            padding: '40px 0 0 0 ',
          }}
        >
          商家注册
        </div>
        <div style={{ margin: '40px 70px' }}>
          <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
          >
            <div style={{}}>
              <Form.Item
                name="Username"
                rules={[{ required: true, message: '请输入用户名!' }]}
              >
                <Input
                  prefix={<UserOutlined className="site-form-item-icon" />}
                  placeholder="请输入用户名"
                  style={{ width: '364px', height: '45px' }}
                />
              </Form.Item>

              <Form.Item
                name="Password"
                rules={[{ required: true, message: '请输入密码!' }]}
              >
                <Input.Password
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  placeholder="请输入密码"
                  style={{ width: '364px', height: '45px' }}
                />
              </Form.Item>

              <Form.Item
                name="rePassword"
                rules={[{ required: true, message: '请输入密码!' }]}
              >
                <Input.Password
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  placeholder="请确认密码"
                  style={{ width: '364px', height: '45px' }}
                />
              </Form.Item>

              <Form.Item
                name="Code"
                rules={[{ required: true, message: '请输入验证码!' }]}
              >
                <div style={{ display: 'flex', width: '365px' }}>
                  <Input
                    placeholder="请输入验证码"
                    style={{ width: '367px', height: '45px' }}
                  />
                  <img src="" style={{ width: 100, height: 45 }} />
                </div>
              </Form.Item>
            </div>
            {/* 记住密码 */}
            {/* <Form.Item name="remember" valuePropName="checked" wrapperCol={{ offset: 8, span: 16 }}>
                        <Checkbox>Remember me</Checkbox>
                    </Form.Item> */}

            <Button
              type="primary"
              htmlType="submit"
              style={{ width: '366px', height: '45px' }}
            >
              注册
            </Button>
            <div style={{ color: 'rgba(0,0,0,.5)', marginLeft: 110 }}>
              已有账号？
              <span style={{ color: '#1890ff' }} onClick={tologin}>
                立即登录
              </span>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
}
