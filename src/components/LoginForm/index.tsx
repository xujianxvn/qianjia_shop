import React, { PureComponent, useState, useEffect } from 'react';
// import $api from '@/http'
import { history } from 'umi';
import { Form, Input, Button, Checkbox, message } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';

export default function login(props: any, state: any) {
  const [url, seturl] = useState('');
  const [codeid, setcodeid] = useState('');
  useEffect(() => {
    myclick();
  }, []);

  const toregister = () => {
    history.replace('Register');
  };

  const myclick = () => {
    //  请求
    // $api.postlist().then(data => {
    //     seturl(data.data.data.img)
    //     setcodeid(data.data.data.id)
    // })
  };

  let [value, setValue] = useState();
  // 成功返回输入值
  const onFinish = (values: any) => {
    console.log('Success:', values);
    // $api.login(values.Username, values.Password, values.Code, codeid).then(data => {
    //   if (data.data.code == 0) {
    //     message.info('登录成功');
    //     localStorage.setItem('token', data.data.data.token)
    //     history.push('@/pages/dianpu/renovation/index')
    //   } else if (data.data.code !== 0) {
    //     message.error(data.data.message);
    //     myclick()
    //   }
    // })
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div style={{ marginTop: '150px' }}>
      <div style={{ textAlign: 'center', fontSize: '30px', color: '#666' }}>
        商家登录
      </div>
      <div style={{ marginLeft: '120px', marginTop: '30px' }}>
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <div style={{ marginLeft: '650px' }}>
            <Form.Item
              name="Username"
              rules={[{ required: true, message: '请输入用户名!' }]}
            >
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="请输入用户名"
                style={{ width: '364px', height: '45px' }}
              />
            </Form.Item>

            <Form.Item
              name="Password"
              rules={[{ required: true, message: '请输入密码!' }]}
            >
              <Input.Password
                prefix={<LockOutlined className="site-form-item-icon" />}
                placeholder="请输入密码"
                style={{ width: '364px', height: '45px' }}
              />
            </Form.Item>

            <Form.Item
              name="Code"
              rules={[{ required: true, message: '请输入验证码!' }]}
            >
              <div style={{ display: 'flex' }}>
                <Input
                  placeholder="请输入验证码"
                  style={{ width: '263px', height: '45px' }}
                />
                <div
                  onClick={myclick}
                  style={{
                    border: '1px solid #d9d9d9',
                    width: '100px',
                    height: '45px',
                  }}
                >
                  <img
                    src={url}
                    alt=""
                    style={{ width: '100%', height: '100%' }}
                  />
                </div>
              </div>
            </Form.Item>
          </div>
          {/* 记住密码 */}
          {/* <Form.Item name="remember" valuePropName="checked" wrapperCol={{ offset: 8, span: 16 }}>
            <Checkbox>Remember me</Checkbox>
          </Form.Item> */}

          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button
              type="primary"
              htmlType="submit"
              style={{ width: '366px', height: '45px', margin: '20px 60px' }}
            >
              登录
            </Button>
            <div style={{ margin: '10px 0 0 120px', color: 'rgba(0,0,0,.5)' }}>
              还没有成为我们的小伙伴？
              <span style={{ color: '#1890ff' }} onClick={toregister}>
                申请入驻
              </span>
            </div>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}

// export interface Props {

// }

// export interface State {
//     value: string;
//     name: string;
// }

// export default class extends PureComponent<Props, State> {

//     constructor(props: Props) {
//         super(props);
//         this.state = {
//             value: '111',
//             name: '',
//         };
//     }

//     // public onInput({ target }: any) {
//     //     this.setState({
//     //         value: target.value,
//     //     });
//     // }

//     public changeValue(value: string) {
//         // console.log(1111, value);
//         this.setState({ value });
//     }

//     public render() {
//         return (
//             <div>
//                 登录 {this.state.value}
//                 <MyInput placeholder="Basic usage" value={this.state.value} changeValue={this.changeValue.bind(this)} />
//             </div>
//         );
//     }
// }
