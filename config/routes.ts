export default [
  { path: '/', name: '概括', component: '@/pages/index' },
  {
    path: '/shop',
    name: '店铺',
    routes: [
      {
        path: 'administration',
        icon: 'ShopOutlined',
        name: '店铺管理',
        routes: [
          {
            path: 'storeinformation',
            name: '店铺信息',
            component: 'shop/administration/StoreInformation',
          },
          {
            path: 'address',
            name: '联系地址',
            component: 'shop/administration/Address',
          },
          {
            path: 'authentication',
            name: '认证信息',
            component: 'shop/administration/Authentication',
          },
          // {
          //   path: 'settled',
          //   name: '入驻申请',
          //   routes: [
          //     { path: 'query', component: 'shop/administration/settled/Query' },
          //     {
          //       path: 'renewal',
          //       component: 'shop/administration/settled/Renewal',
          //     },
          //     {
          //       path: 'authentication',
          //       component: 'shop/administration/settled/Authentication',
          //     },
          //     { redirect: '/shop/administration/settled/Query' },
          //   ],
          // },
        ],
      },
      { redirect: 'administration/StoreInformation' },
    ],
  },
  {
    path: '/commodity',
    name: '商品',
    routes: [
      {
        icon: 'AppstoreOutlined',
        path: 'list',
        name: '商品列表',
        component: 'commodity/List',
      },
      // {
      //   icon: 'AppstoreOutlined',
      //   path: 'category',
      //   name: '店内分类',
      //   component: 'commodity/Category',
      // },
      // {
      //   icon: 'AppstoreOutlined',
      //   path: 'evaluate',
      //   name: '商品评价',
      //   component: 'commodity/Evaluate',
      // },
      // {
      //   icon: 'AppstoreOutlined',
      //   path: 'parameter',
      //   name: '商品参数',
      //   component: 'commodity/Parameter',
      // },
      {
        icon: 'AppstoreOutlined',
        path: 'administration',
        name: '品牌管理',
        component: 'commodity/Administration',
      },
      {
        icon: 'AppstoreOutlined',
        path: 'recycleBin',
        name: '回收站',
        component: 'commodity/RecycleBin',
      },
      // {
      //   icon: 'AppstoreOutlined',
      //   path: 'album',
      //   name: '相册管理',
      //   component: 'commodity/Album',
      // },
      { redirect: '/commodity/List' },
    ],
  },
  {
    path: '/order',
    name: '订单',
    routes: [
      {
        icon: 'AppstoreOutlined',
        path: 'administration',
        name: '订单管理',
        component: 'order/Administration',
      },
      {
        icon: 'AppstoreOutlined',
        path: 'shipment',
        name: '订单发货',
        component: 'order/Shipment',
      },
      {
        icon: 'AppstoreOutlined',
        path: 'refund',
        name: '退款维权',
        component: 'order/Refund',
      },
      { redirect: '/order/Administration' },
    ],
  },
  {
    path: '/member',
    name: '会员',
    routes: [
      {
        icon: 'AppstoreOutlined',
        path: 'profile',
        name: '会员概况',
        component: 'member/Profile',
      },
      {
        icon: 'AppstoreOutlined',
        path: 'administration',
        name: '会员管理',
        component: 'member/Administration',
      },
      { redirect: '/member/Profile' },
    ],
  },
  // {
  //   path: 'marketing',
  //   name: '营销',
  //   routes: [
  //     {
  //       icon: 'AppstoreOutlined',
  //       path: 'marketingCenter',
  //       name: '营销中心',
  //       component: 'marketing/MarketingCenter',
  //     },
  //     {
  //       icon: 'AppstoreOutlined',
  //       path: 'memberMarketing',
  //       name: '会员营销',
  //       component: 'marketing/MemberMarketing',
  //     },
  //     { redirect: '/marketing/MarketingCenter' },
  //   ],
  // },
  // {
  //   path: 'assets',
  //   name: '资产',
  //   routes: [
  //     {
  //       icon: 'AppstoreOutlined',
  //       path: 'overview',
  //       name: '资产概况',
  //       routes: [
  //         {
  //           icon: 'AppstoreOutlined',
  //           path: 'Overview',
  //           component: 'assets/overview/Overview',
  //         },
  //         {
  //           icon: 'AppstoreOutlined',
  //           path: 'Detail',
  //           component: 'assets/overview/Detail',
  //         },
  //         { redirect: '/assets/overview/Overview' },
  //       ],
  //     },
  //     {
  //       icon: 'AppstoreOutlined',
  //       path: 'storeSettlement',
  //       name: '店铺结算',
  //       routes: [
  //         {
  //           icon: 'AppstoreOutlined',
  //           path: 'StoreSettlement',
  //           component: 'assets/storeSettlement/StoreSettlement',
  //         },
  //         {
  //           icon: 'AppstoreOutlined',
  //           path: 'StoreDetail',
  //           component: 'assets/storeSettlement/StoreDetail',
  //         },
  //         { redirect: '/assets/storeSettlement/StoreSettlement' },
  //       ],
  //     },
  //     {
  //       icon: 'AppstoreOutlined',
  //       path: 'shopSettlement',
  //       name: '门店结算',
  //       routes: [
  //         {
  //           icon: 'AppstoreOutlined',
  //           path: 'ShopSettlement',
  //           component: 'assets/shopSettlement/ShopSettlement',
  //         },
  //         {
  //           icon: 'AppstoreOutlined',
  //           path: 'ShopDetail',
  //           component: 'assets/shopSettlement/ShopDetail',
  //         },
  //         { redirect: '/assets/shopSettlement/ShopSettlement' },
  //       ],
  //     },
  //     { redirect: '/assets/shopSettlement/ShopSettlement' },
  //   ],
  // },
  // {
  //   path: 'statistics',
  //   name: '统计',
  //   routes: [
  //     {
  //       icon: 'AppstoreOutlined',
  //       path: 'storeStatistics',
  //       name: '店铺统计',
  //       component: 'statistics/StoreStatistics',
  //     },
  //     {
  //       icon: 'AppstoreOutlined',
  //       path: 'commodityStatistics',
  //       name: '商品统计',
  //       component: 'statistics/CommodityStatistics',
  //     },
  //     {
  //       icon: 'AppstoreOutlined',
  //       path: 'transactionStatistics',
  //       name: '交易统计',
  //       component: 'statistics/TransactionStatistics',
  //     },
  //     {
  //       icon: 'AppstoreOutlined',
  //       path: 'accessStatistics',
  //       name: '访问统计',
  //       component: 'statistics/AccessStatistics',
  //     },
  //     { redirect: '/statistics/StoreStatistics' },
  //   ],
  // },
  {
    path: '/assembly',
    name: '组件',
    routes: [
      {
        icon: 'AppstoreOutlined',
        path: 'form',
        name: '表单',
        component: 'assembly/Form',
      },
      {
        icon: 'AppstoreOutlined',
        path: 'table',
        name: '表单',
        component: 'assembly/Table',
      },
      { redirect: '/assembly/form' },
    ],
  },
  {
    path: '/Login',
    component: '@/pages/Login',
    layout: {
      // hideMenu: false,
      hideNav: true,
      // hideFooter: false,
    },
  },
  {
    path: '/Register',
    component: '@/pages/Register',
    layout: {
      // hideMenu: false,
      hideNav: true,
      // hideFooter: false,
    },
  },
];
