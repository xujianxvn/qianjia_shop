import { defineConfig } from 'umi';
import routes from './routes';

export default defineConfig({
  antd: {
    dark: false, // 开启暗色主题
    compact: false, // 开启紧凑主题
  },
  nodeModulesTransform: {
    type: 'none',
  },

  layout: {
    // 支持任何不需要 dom 的
    // https://procomponents.ant.design/components/layout#prolayout
    name: '仟佳商城',
    locale: true,
    layout: 'mix',
    splitMenus: true,
    navTheme: 'light',
    headerTheme: 'light',
  },

  routes: routes,
  fastRefresh: {},
});
